CREATE TABLE basket (
     client_id uuid NOT NULL REFERENCES client(user_id) ON DELETE CASCADE PRIMARY KEY,
     name varchar NOT NULL
);