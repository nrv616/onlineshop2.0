CREATE TABLE "user" (
    id                 uuid         NOT NULL,
    login              varchar(50)  NOT NULL,
    "password"         varchar(60)  NULL,
    "name"             varchar(200) NULL,
    is_active          bool         NOT NULL,
    is_technical       bool         NOT NULL,
    CONSTRAINT pk_user PRIMARY KEY (id),
    CONSTRAINT user_login_unique UNIQUE (login)
);

CREATE TABLE user_role (
    user_id   uuid NOT NULL,
    role_id   uuid NOT NULL,
    is_active bool NULL,
    CONSTRAINT pk_user_role PRIMARY KEY (user_id, role_id)
);

CREATE TABLE "role" (
    id uuid NOT NULL,
    "name" varchar(255) NULL,
    is_technical bool NOT NULL,
    is_active bool NOT NULL,
    CONSTRAINT pk_role PRIMARY KEY (id)
);

CREATE TABLE role_permission (
    role_id uuid NOT NULL,
    permission_id varchar(50) NOT NULL,
    id uuid NOT NULL,
    permission_status varchar(20) NOT NULL,
    CONSTRAINT pk_role_permission PRIMARY KEY (id)
);

CREATE TABLE "permission" (
    id varchar(50) NOT NULL,
    description varchar(255) NULL,
    short_id varchar(10) NULL,
    is_technical bool NOT NULL,
    CONSTRAINT permission_short_id_unique UNIQUE (short_id),
    CONSTRAINT pk_permission PRIMARY KEY (id)
);

ALTER TABLE role_permission ADD CONSTRAINT role_permission_role_id_permission_id_unique UNIQUE (role_id, permission_id);
ALTER TABLE role_permission ADD CONSTRAINT fk_role_permission_permission_id_to_permission FOREIGN KEY (permission_id) REFERENCES permission(id);
ALTER TABLE role_permission ADD CONSTRAINT fk_role_permission_role_id_to_role FOREIGN KEY (role_id) REFERENCES role(id);

ALTER TABLE user_role ADD CONSTRAINT fk_user_role_role_id_to_role FOREIGN KEY (role_id) REFERENCES role(id);
ALTER TABLE user_role ADD CONSTRAINT fk_user_role_user_id_to_user FOREIGN KEY (user_id) REFERENCES "user"(id);