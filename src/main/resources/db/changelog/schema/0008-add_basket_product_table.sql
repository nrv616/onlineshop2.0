CREATE TABLE basket_product (
    id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
    product_id int4 NOT NULL REFERENCES product (id)  ON DELETE CASCADE,
    basket_id UUID NOT NULL REFERENCES basket (client_id)  ON DELETE CASCADE,
    product_count int4
);