CREATE TABLE category (
    id SERIAL PRIMARY KEY,
    name varchar NOT NULL UNIQUE,
    parent_id int4 REFERENCES category (id) ON DELETE CASCADE
);