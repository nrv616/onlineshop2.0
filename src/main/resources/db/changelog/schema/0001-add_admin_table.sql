CREATE TABLE admin (
    user_id UUID NOT NULL REFERENCES "user" (id) ON DELETE CASCADE PRIMARY KEY,
	position varchar(200) NOT NULL
);