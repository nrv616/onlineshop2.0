CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE deposit (
    client_id uuid PRIMARY KEY  NOT NULL REFERENCES client (user_id)  ON DELETE CASCADE,
	deposit int4 NOT NULL
);