CREATE TABLE product_category (
    product_id int4 NOT NULL REFERENCES product (id) ON DELETE CASCADE,
    category_id int4 NOT NULL REFERENCES category (id) ON DELETE CASCADE
);