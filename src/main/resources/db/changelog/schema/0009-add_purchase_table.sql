CREATE TABLE purchase (
    id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
    client_id UUID NOT NULL REFERENCES client (user_id)  ON DELETE CASCADE,
    product_id int4  NOT NULL REFERENCES product (id)  ON DELETE CASCADE,
    name varchar NOT NULL,
    price int4,
    product_count int4
);