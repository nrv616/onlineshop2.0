CREATE TABLE product (
    id SERIAL PRIMARY KEY,
    name varchar NOT NULL,
    price int4 NOT NULL,
    product_count int4 NOT NULL
);