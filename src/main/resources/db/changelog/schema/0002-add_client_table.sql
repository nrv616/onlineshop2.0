CREATE TABLE client (
    user_id UUID NOT NULL REFERENCES "user" (id) ON DELETE CASCADE PRIMARY KEY,
	email varchar(200) NOT NULL,
	address varchar(200) NOT NULL,
	phone varchar(200) NOT NULL
);