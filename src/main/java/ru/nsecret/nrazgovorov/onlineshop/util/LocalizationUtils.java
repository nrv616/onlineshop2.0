package ru.nsecret.nrazgovorov.onlineshop.util;

import java.util.Locale;

public class LocalizationUtils {

    public static Locale createLocaleFromString(String str) {
        String separator = null;
        if (str.contains("-")) {
            separator = "-";
        } else if (str.contains("_")) {
            separator = "_";
        }

        if (separator != null) {
            String[] langCountry = str.split(separator);
            String language = langCountry[0].toLowerCase();
            String country = langCountry[1].toUpperCase();
            return new Locale(language, country);
        } else {
            return new Locale(str);
        }
    }
}
