package ru.nsecret.nrazgovorov.onlineshop.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.exception.AuthenticationException;
import ru.nsecret.nrazgovorov.onlineshop.repository.UserRepository;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {
        return userRepository.findByLogin(username)
                .map(UserDetailsAdapter::new)
                .orElseThrow(() -> new AuthenticationException("A000005", username));
    }
}
