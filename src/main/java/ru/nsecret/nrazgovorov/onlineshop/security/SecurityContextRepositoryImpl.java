package ru.nsecret.nrazgovorov.onlineshop.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.stereotype.Component;
import ru.nsecret.nrazgovorov.onlineshop.exception.AuthenticationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;
import static org.testcontainers.shaded.org.apache.commons.lang.StringUtils.removeStart;
import static ru.nsecret.nrazgovorov.onlineshop.security.Claim.JWT_CLAIM_LOGIN;

@Component
@RequiredArgsConstructor
@Slf4j
public class SecurityContextRepositoryImpl implements SecurityContextRepository {

    private static final String BEARER = "Bearer";
    private final CustomUserDetailsService userDetailsService;

    /**
     * Executes before each request to process http 'Authorization' header and stores its value
     * in SecurityContext
     */
    @Override
    public SecurityContext loadContext(HttpRequestResponseHolder httpRequestResponseHolder) {
        return extractUserFromRequest(httpRequestResponseHolder.getRequest())
                .map(userDetails ->
                        new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities())
                )
                .map(SecurityContextImpl::new)
                .orElse(new SecurityContextImpl());
    }

    /**
     * Parses JWT token
     */
    private Optional<UserDetailsAdapter> extractUserFromRequest(HttpServletRequest request) {
        return ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION))
                .map(header -> removeStart(header, BEARER).trim())
                .map(this::detailsFromToken);
    }

    @Override
    public void saveContext(
            SecurityContext securityContext,
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse
    ) {
        //do not need to save context
    }

    @Override
    public boolean containsContext(HttpServletRequest httpServletRequest) {
        return false;
    }

    private UserDetailsAdapter detailsFromToken(String jwtToken) {
        DecodedJWT decodedJWT = validateToken(jwtToken);
        Map<String, com.auth0.jwt.interfaces.Claim> payload = decodedJWT.getClaims();
        return (UserDetailsAdapter) userDetailsService.loadUserByUsername(payload.get(JWT_CLAIM_LOGIN.getValue()).asString());
    }

    private DecodedJWT validateToken(String jwtToken) {
        Algorithm algorithm = Algorithm.HMAC256("secret");
        JWTVerifier jwtVerifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT;
        try {
            decodedJWT = jwtVerifier.verify(jwtToken);
        } catch (JWTVerificationException e) {
            if (e instanceof TokenExpiredException) {
                throw new AuthenticationException("A000003");
            }
            if (e instanceof SignatureVerificationException) {
                throw new AuthenticationException("A000002");
            } else {
                throw new AuthenticationException("A000001");
            }
        }
        return decodedJWT;
    }
}
