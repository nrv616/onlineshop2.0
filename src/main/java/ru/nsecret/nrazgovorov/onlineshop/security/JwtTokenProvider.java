package ru.nsecret.nrazgovorov.onlineshop.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.nsecret.nrazgovorov.onlineshop.rest.auth.dto.JwtResponse;

import java.util.Objects;

import static ru.nsecret.nrazgovorov.onlineshop.security.Claim.*;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {

    @Value("${application.auth.token-validity-seconds}")
    private int tokenValiditySeconds;

    public JwtResponse generateToken(UserDetailsAdapter adapter) throws Exception {
        JwtPayload payload = createJwtPayloadFromUser(adapter);

        Algorithm algorithm = Algorithm.HMAC256("secret");
        long expiryTimeSeconds = System.currentTimeMillis() / 1000L + tokenValiditySeconds;

        JWTCreator.Builder builder = JWT.create()
                .withClaim(JWT_CLAIM_ID.getValue(), payload.getId())
                .withClaim(JWT_CLAIM_LOGIN.getValue(), payload.getLogin())
                .withClaim(JWT_CLAIM_EXPIRATION.getValue(), expiryTimeSeconds)
                .withClaim(JWT_CLAIM_LOCALE.getValue(), payload.getLocale() == null ? null : payload.getLocale().toString())
                .withClaim(JWT_CLAIM_IS_TECHNICAL.getValue(), payload.getIsTechnical());

        if (payload.getRoles() != null) {
            String[] permissionIds = payload.getRoles().stream()
                    .filter(ur -> ur.getPermissions() != null)
                    .flatMap(ur -> ur.getPermissions().stream())
                    .map(rp -> rp.getPermission().getShortId())
                    .filter(Objects::nonNull)
                    .distinct()
                    .toArray(String[]::new);

            builder.withArrayClaim(JWT_CLAIM_PERMISSIONS.getValue(), permissionIds);
        }

        String token = builder.sign(algorithm);
        return new JwtResponse(adapter.getUser().getLogin(),
                String.valueOf(expiryTimeSeconds), String.valueOf(tokenValiditySeconds), token);
    }

    private static JwtPayload createJwtPayloadFromUser(UserDetailsAdapter adapter) {
        JwtPayload p = new JwtPayload();
        p.setId(adapter.getUser().getId() == null ? null : adapter.getUser().getId().toString());
        p.setLogin(adapter.getUser().getLogin());
        p.setName(adapter.getUser().getName());
        p.setRoles(adapter.getUser().getActiveRoles());
        p.setIsTechnical(adapter.getUser().isTechnical());
        return p;
    }
}
