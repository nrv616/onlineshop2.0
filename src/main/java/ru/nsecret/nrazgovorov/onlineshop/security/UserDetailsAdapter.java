package ru.nsecret.nrazgovorov.onlineshop.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.nsecret.nrazgovorov.onlineshop.model.User;

import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class UserDetailsAdapter implements UserDetails {

    private User user;

    private String accessToken;

    public UserDetailsAdapter(User user) {
        this.user = user;
    }

    @Override
    public Set<GrantedAuthority> getAuthorities() {
        return user.getActivePermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getId()))
                .collect(toSet());
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.isActive();
    }

    public String getName() {
        return user.getName();
    }

    public User getUser() {
        return user;
    }

    public String getAccessToken() {
        return accessToken;
    }

    @SuppressWarnings("WeakerAccess")
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
