package ru.nsecret.nrazgovorov.onlineshop.security;

import com.auth0.jwt.exceptions.JWTDecodeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.nsecret.nrazgovorov.onlineshop.exception.AuthenticationException;

import javax.servlet.http.HttpServletRequest;
import java.time.ZonedDateTime;

@RestControllerAdvice
public class AuthenticationExceptionsHandler {

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseEntity<CommonErrorDto> handleAuthenticationException(HttpServletRequest request, AuthenticationException exception) {
        return processAuthenticationError(request, exception.getMessage());
    }

    @ExceptionHandler(JWTDecodeException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseEntity<CommonErrorDto> handleException(HttpServletRequest request, JWTDecodeException exception) {
        return processAuthenticationError(request, exception.getMessage());
    }

    private ResponseEntity<CommonErrorDto> processAuthenticationError(HttpServletRequest request, String message) {
        HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
        CommonErrorDto commonErrorDto = new CommonErrorDto();
        commonErrorDto.setTimestamp(ZonedDateTime.now());
        commonErrorDto.setPath(request.getRequestURI());
        commonErrorDto.setStatus(httpStatus.value());
        commonErrorDto.setMessage(message);

        return new ResponseEntity<>(commonErrorDto, httpStatus);
    }
}
