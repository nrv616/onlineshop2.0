package ru.nsecret.nrazgovorov.onlineshop.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.ZonedDateTime;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class CommonErrorDto {

    private ZonedDateTime timestamp;
    private Integer status;
    private String error;
    private String message;
    private String path;
}
