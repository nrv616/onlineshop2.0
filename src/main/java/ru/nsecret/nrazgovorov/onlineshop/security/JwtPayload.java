package ru.nsecret.nrazgovorov.onlineshop.security;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsecret.nrazgovorov.onlineshop.model.Role;

import java.util.List;
import java.util.Locale;

/**
 * DTO with data that we need to have in JWT token.
 * Use this to decouple JWT code from {@link org.springframework.security.core.userdetails.UserDetails}
 */
@Getter
@Setter
@NoArgsConstructor
public class JwtPayload {

    private String id;
    private String login;
    private String name;
    private Locale locale;
    private Long expirationTimeSeconds;
    private Boolean isTechnical;
    private List<Role> roles;
}
