package ru.nsecret.nrazgovorov.onlineshop.security;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;

import javax.servlet.http.HttpServletResponse;

public class SecurityMappings extends AbstractHttpConfigurer<SecurityMappings, HttpSecurity> {

    @Override
    public void init(HttpSecurity builder) throws Exception {

        builder.authorizeRequests()
                //.antMatchers(GET, "/countries/**").hasAuthority("R_CNTR")
                .anyRequest().authenticated()
                .and()
                .exceptionHandling()
                .authenticationEntryPoint((request, response, exception) -> {
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    response.getWriter().println("401 - Unauthorized");
                });
    }

    public static SecurityMappings securityMappings() {
        return new SecurityMappings();
    }
}
