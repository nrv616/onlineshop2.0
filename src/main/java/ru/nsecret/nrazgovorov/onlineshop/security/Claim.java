package ru.nsecret.nrazgovorov.onlineshop.security;

public enum Claim {
    JWT_CLAIM_ID("id"),
    JWT_CLAIM_LOGIN("l"),
    JWT_CLAIM_EXPIRATION("exp"),
    JWT_CLAIM_LOCALE("locale"),
    JWT_CLAIM_PERMISSIONS("p"),
    JWT_CLAIM_IS_TECHNICAL("t");

    private String value;

    private Claim(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
