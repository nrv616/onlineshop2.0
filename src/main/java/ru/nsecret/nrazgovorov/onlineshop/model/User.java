package ru.nsecret.nrazgovorov.onlineshop.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "`user`", uniqueConstraints = {
        @UniqueConstraint(
                columnNames = {"login"},
                name = "user_login_unique"
        )
})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @NotNull
    @Column(name = "login", length = 50)
    private String login;

    @Column(name = "password", length = 50)
    private String password;

    @Column(name = "name", length = 200)
    private String name;

    @OneToMany(mappedBy = "user")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<UserRole> roles;

    @Column(name = "is_active")
    private boolean isActive;

    @Column(name = "is_technical")
    private boolean isTechnical;

    public List<Role> getActiveRoles() {
        if (roles == null) return Collections.emptyList();
        return roles.stream()
                .filter(userRole -> userRole.getRole().isActive())
                .filter(UserRole::getIsActive)
                .map(UserRole::getRole)
                .collect(toList());
    }

    public Set<UserPermission> getActivePermissions() {
        if (this.roles == null) return emptySet();
        return this.roles.stream()
                .filter(userRole -> userRole.getRole().isActive())
                .filter(UserRole::getIsActive)
                .flatMap(userRole -> userRole.getRole().getPermissions().stream())
                .filter(rp -> rp.getStatus().equals(RolePermission.Status.ACTIVE)
                        || rp.getStatus().equals(RolePermission.Status.FOR_DELETE))
                .map(RolePermission::getPermission)
                .collect(toSet());
    }

}
