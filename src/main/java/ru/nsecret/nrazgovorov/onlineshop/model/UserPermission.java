package ru.nsecret.nrazgovorov.onlineshop.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@ToString(of = "id")
@Entity
@Table(name = "`permission`")
public class UserPermission {

    @Id
    @Column(name = "id", length = 50)
    private String id;

    @Column(name = "description", length = 255)
    private String description;

    @Column(name = "short_id", length = 10)
    private String shortId;

    @Column(name = "is_technical")
    private boolean isTechnical;
}
