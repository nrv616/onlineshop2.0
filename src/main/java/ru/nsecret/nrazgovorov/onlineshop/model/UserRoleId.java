package ru.nsecret.nrazgovorov.onlineshop.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Embeddable
@EqualsAndHashCode
public class UserRoleId implements Serializable {

    private static final long serialVersionUID = 2580350181773158575L;

    @Column(name = "user_id")
    @Type(type = "pg-uuid")
    private UUID userId;

    @Column(name = "role_id")
    @Type(type = "pg-uuid")
    private UUID roleId;
}
