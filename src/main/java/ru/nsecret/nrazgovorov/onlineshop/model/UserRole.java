package ru.nsecret.nrazgovorov.onlineshop.model;


import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.util.Objects;

@Getter
@Entity
@EqualsAndHashCode(of = "id")
@Table(name = "user_role")
public class UserRole {

    @Getter(AccessLevel.NONE)
    @EmbeddedId
    private UserRoleId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("roleId")
    @JoinColumn(name = "role_id")
    private Role role;

    @Setter
    @Column(name = "is_active")
    private Boolean isActive;

    public UserRole() {
        id = new UserRoleId();
    }

    public UserRole(User user, Role role, boolean isActive) {
        Objects.requireNonNull(user);
        Objects.requireNonNull(role);

        this.id = new UserRoleId(user.getId(), role.getId());
        this.user = user;
        this.role = role;
        this.isActive = isActive;
    }
}
