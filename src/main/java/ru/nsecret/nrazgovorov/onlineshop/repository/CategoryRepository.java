package ru.nsecret.nrazgovorov.onlineshop.repository;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;
import ru.nsecret.nrazgovorov.onlineshop.model.Category;

import java.util.UUID;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>,
        QuerydslPredicateExecutor<Category>, QuerydslBinderCustomizer {

    @Override
    default void customize(QuerydslBindings bindings, EntityPath entityPath) {
        bindings.bind(String.class).first(
                (SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase
        );
    }

}
