package ru.nsecret.nrazgovorov.onlineshop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class AuthenticationException extends CommonException {

    private static final long serialVersionUID = 7021374800700121318L;

    public AuthenticationException(String errorCode, Object... messageArguments) {
        super(errorCode, messageArguments);
    }
}
