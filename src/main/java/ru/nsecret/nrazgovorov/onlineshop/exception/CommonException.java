package ru.nsecret.nrazgovorov.onlineshop.exception;

import lombok.Getter;

@Getter
public abstract class CommonException extends RuntimeException {

    private final String errorCode;
    private Object[] messageArguments;

    public CommonException(String errorCode, Object... messageArguments) {
        this.errorCode = errorCode;
        this.messageArguments = messageArguments;
    }

    public CommonException(Throwable cause, String errorCode, Object... messageArguments) {
        super(cause);
        this.errorCode = errorCode;
        this.messageArguments = messageArguments;
    }
}
