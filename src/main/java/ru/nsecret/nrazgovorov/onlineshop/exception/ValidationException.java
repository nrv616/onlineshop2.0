package ru.nsecret.nrazgovorov.onlineshop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ValidationException extends CommonException {

    public ValidationException(String errorCode, Object... messageArguments) {
        super(errorCode, messageArguments);
    }
}
