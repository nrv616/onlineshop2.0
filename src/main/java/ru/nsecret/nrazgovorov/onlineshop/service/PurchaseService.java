package ru.nsecret.nrazgovorov.onlineshop.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.exception.NotFoundException;
import ru.nsecret.nrazgovorov.onlineshop.model.Client;
import ru.nsecret.nrazgovorov.onlineshop.model.Product;
import ru.nsecret.nrazgovorov.onlineshop.model.Purchase;
import ru.nsecret.nrazgovorov.onlineshop.repository.ClientRepository;
import ru.nsecret.nrazgovorov.onlineshop.repository.ProductRepository;
import ru.nsecret.nrazgovorov.onlineshop.repository.PurchaseRepository;
import ru.nsecret.nrazgovorov.onlineshop.rest.purchase.dto.PurchaseDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.purchase.dto.PurchaseSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.purchase.mapper.PurchaseMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PurchaseService {

    private final PurchaseRepository purchaseRepository;

    private final ClientRepository clientRepository;

    private final ProductRepository productRepository;

    private final PurchaseMapper purchaseMapper;

    public List<PurchaseDto> getAllPurchases() {
        List<PurchaseDto> purchases = new ArrayList<>();
        for (Purchase purchase : purchaseRepository.findAll()) {
            purchases.add(purchaseMapper.purchaseToPurchaseDto(purchase));
        }
        return purchases;
    }

    public List<PurchaseDto> getAllPurchasesPaging(Pageable pageable) {
        List<PurchaseDto> purchases = new ArrayList<>();
        for (Purchase purchase : purchaseRepository.findAll(pageable)) {
            purchases.add(purchaseMapper.purchaseToPurchaseDto(purchase));
        }
        return purchases;
    }

    public PurchaseDto getPurchaseById(UUID id) {
        return purchaseMapper.purchaseToPurchaseDto(purchaseRepository.findById(id).orElseThrow(NotFoundException::new));
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public PurchaseDto createPurchase(PurchaseSaveDto purchaseSaveDto) {
        Purchase purchase1 = purchaseMapper.purchaseSaveDtoToPurchase(purchaseSaveDto);
        Client client = clientRepository.findById(purchaseSaveDto.getClientId()).orElseThrow(NotFoundException::new);
        Product product = productRepository.findById(purchase1.getProduct().getId()).orElseThrow(NotFoundException::new);
        purchase1.setClient(client);
        purchase1.setProduct(product);
        Purchase purchase = purchaseRepository.save(purchase1);
        return purchaseMapper.purchaseToPurchaseDto(purchase);
    }

    public PurchaseDto updatePurchase(PurchaseSaveDto updatePurchaseRequest, UUID id) {
        Purchase purchaseOld = purchaseRepository.findById(id).orElseThrow(NotFoundException::new);
        return purchaseMapper.purchaseToPurchaseDto(
                purchaseRepository.save(purchaseOld));
    }

    public void deletePurchaseById(UUID id) {
        purchaseRepository.delete(purchaseRepository.findById(id).orElseThrow(NotFoundException::new));
    }
}
