package ru.nsecret.nrazgovorov.onlineshop.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.nsecret.nrazgovorov.onlineshop.exception.CommonException;
import ru.nsecret.nrazgovorov.onlineshop.exception.NotFoundException;
import ru.nsecret.nrazgovorov.onlineshop.exception.ValidationException;
import ru.nsecret.nrazgovorov.onlineshop.model.Basket;
import ru.nsecret.nrazgovorov.onlineshop.model.BasketProduct;
import ru.nsecret.nrazgovorov.onlineshop.model.Client;
import ru.nsecret.nrazgovorov.onlineshop.model.Product;
import ru.nsecret.nrazgovorov.onlineshop.model.QBasket;
import ru.nsecret.nrazgovorov.onlineshop.model.User;
import ru.nsecret.nrazgovorov.onlineshop.repository.BasketProductRepository;
import ru.nsecret.nrazgovorov.onlineshop.repository.BasketRepository;
import ru.nsecret.nrazgovorov.onlineshop.repository.ClientRepository;
import ru.nsecret.nrazgovorov.onlineshop.rest.basket.dto.BasketDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.basket.dto.BasketProductDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.basket.dto.BasketSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.basket.mapper.BasketMapper;
import ru.nsecret.nrazgovorov.onlineshop.rest.basket.mapper.BasketProductMapper;
import ru.nsecret.nrazgovorov.onlineshop.rest.product.dto.ProductDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.product.mapper.ProductMapper;
import ru.nsecret.nrazgovorov.onlineshop.rest.purchase.dto.PurchaseRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class BasketService {

    private final BasketRepository basketRepository;

    private final BasketProductRepository basketProductRepository;

    private final ClientRepository clientRepository;

    private final ProductService productService;

    private final UserService userService;

    private final BasketMapper basketMapper;

    private final ProductMapper productMapper;

    private final BasketProductMapper basketProductMapper;

    public List<BasketDto> getAllBasketsPaging(Pageable pageable) {
        List<BasketDto> baskets = new ArrayList<>();
        for (Basket basket : basketRepository.findAll(pageable)) {
            baskets.add(basketMapper.basketToBasketDto(basket));
        }
        return baskets;
    }

    public Basket findByClientId(UUID id) {
        return StreamSupport.stream(basketRepository.findAll(QBasket.basket.clientId.eq(id)).spliterator(), false)
                .findFirst().get();
    }

    public List<ProductDto> getAllProducts(UUID basketId) {
        List<ProductDto> products = new ArrayList<>();
        for (BasketProduct basketProduct : basketRepository.findById(basketId).orElseThrow(NotFoundException::new).getProducts()) {
            Product product = basketProduct.getProduct();
            product.setCount(basketProduct.getCount());
            products.add(productMapper.productToProductDto(product));
        }
        return products;
    }

    public List<ProductDto> getAllProductsPaging(UUID basketId, Pageable pageable) {
        List<ProductDto> products = new ArrayList<>();
        for (BasketProduct basketProduct : basketProductRepository.findAllByBasketClientId(basketId, pageable)) {
            Product product = basketProduct.getProduct();
            product.setCount(basketProduct.getCount());
            products.add(productMapper.productToProductDto(product));
        }
        return products;
    }

    public BasketDto getBasketById(UUID id) {
        Basket basket = basketRepository.findById(id).orElseThrow(NotFoundException::new);
        return basketMapper.basketToBasketDto(basket);
    }

    public BasketDto createBasket(BasketSaveDto basketSaveDto) {
        Basket basket = basketMapper.basketSaveDtoToBasket(basketSaveDto);
        Client client = clientRepository.findById(basketSaveDto.getClientId()).orElseThrow(NotFoundException::new);
        basket.setClientId(client.getId());
        basket.setName(basketSaveDto.getName());
        return basketMapper.basketToBasketDto(basketRepository.save(basket));
    }

    public BasketDto updateBasket(BasketSaveDto updateBasketRequest, UUID id) {
        Basket basketOld = basketRepository.findById(id).orElseThrow(NotFoundException::new);
        return basketMapper.basketToBasketDto(
                basketRepository.save(basketOld));
    }

    public void deleteBasketById(UUID id) {
        basketRepository.delete(basketRepository.findById(id).orElseThrow(NotFoundException::new));
    }

    public List<BasketProductDto> addProductToBasket(PurchaseRequest purchaseRequest) throws CommonException {
        Product productTemp = productService.findById(purchaseRequest.getId());
        if (!purchaseRequest.getName().equals(productTemp.getName())) {
            throw new ValidationException("PP00001", purchaseRequest.getName(), productTemp.getName());
        }
        if (purchaseRequest.getPrice() != productTemp.getPrice()) {
            throw new ValidationException("PP00001", purchaseRequest.getPrice(), productTemp.getPrice());
        }
        Optional<User> user = userService.getCurrentUser();
        Basket basket = findByClientId(user.get().getId());
        basket.getProducts().add(new BasketProduct(null, productTemp, basket, purchaseRequest.getCount()));
        List<BasketProductDto> result = new ArrayList<>();
        basket.getProducts().forEach(basketProduct -> result.add(basketProductMapper.basketProductToBasketProductDto(basketProduct)));
        return result;
    }

    public void deleteProduct(UUID id) throws CommonException {
        Optional<User> user = userService.getCurrentUser();
        Basket basket = findByClientId(user.get().getId());
        BasketProduct basketProductToDelete = basket.getProducts().stream().filter(basketProduct -> basketProduct.getProduct().getId().equals(id)).findFirst().get();
        basketProductRepository.delete(basketProductToDelete);
    }

    public List<BasketProductDto> editProductFromBasket(PurchaseRequest purchaseRequest) throws CommonException {
        validatePurchaseRequest(purchaseRequest);
        Optional<User> user = userService.getCurrentUser();
        Basket basket = findByClientId(user.get().getId());
        BasketProduct basketProductToUpdate = basket.getProducts().stream()
                .filter(basketProduct -> basketProduct.getProduct().getId()
                        .equals(purchaseRequest.getId())).findFirst().get();
        basketProductToUpdate.setCount(purchaseRequest.getCount());
        basketProductRepository.save(basketProductToUpdate);
        List<BasketProductDto> result = new ArrayList<>();
        basket.getProducts().forEach(basketProduct -> result.add(basketProductMapper.basketProductToBasketProductDto(basketProduct)));
        return result;
    }

    public List<BasketProductDto> getProductsFromBasket() throws CommonException {
        Optional<User> user = userService.getCurrentUser();
        Basket basket = findByClientId(user.get().getId());
        List<BasketProductDto> result = new ArrayList<>();
        basket.getProducts().forEach(basketProduct -> result.add(basketProductMapper.basketProductToBasketProductDto(basketProduct)));
        return result;
    }

    private void validatePurchaseRequest(PurchaseRequest purchaseRequest) {
        Product productTemp = productService.findById(purchaseRequest.getId());
        if (!purchaseRequest.getName().equals(productTemp.getName())) {
            throw new ValidationException("PP00001", purchaseRequest.getName(), productTemp.getName());
        }
        if (purchaseRequest.getPrice() != productTemp.getPrice()) {
            throw new ValidationException("PP00001", purchaseRequest.getPrice(), productTemp.getPrice());
        }
    }
}
