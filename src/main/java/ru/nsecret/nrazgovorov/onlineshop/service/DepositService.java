package ru.nsecret.nrazgovorov.onlineshop.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.exception.NotFoundException;
import ru.nsecret.nrazgovorov.onlineshop.model.Client;
import ru.nsecret.nrazgovorov.onlineshop.model.Deposit;
import ru.nsecret.nrazgovorov.onlineshop.repository.ClientRepository;
import ru.nsecret.nrazgovorov.onlineshop.repository.DepositRepository;
import ru.nsecret.nrazgovorov.onlineshop.rest.deposit.dto.DepositDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.deposit.dto.DepositSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.deposit.mapper.DepositMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DepositService {

    private final DepositRepository depositRepository;

    private final ClientRepository clientRepository;

    private final DepositMapper depositMapper;

    public List<DepositDto> getAllDeposits() {
        List<DepositDto> deposits = new ArrayList<>();
        for (Deposit deposit : depositRepository.findAll()) {
            deposits.add(depositMapper.depositToDepositDto(deposit));
        }
        return deposits;
    }

    public List<DepositDto> getAllDepositsPaging(Pageable pageable) {
        List<DepositDto> deposits = new ArrayList<>();
        for (Deposit deposit : depositRepository.findAll(pageable)) {
            deposits.add(depositMapper.depositToDepositDto(deposit));
        }
        return deposits;
    }

    public DepositDto getDepositById(UUID id) {
        return depositMapper.depositToDepositDto(depositRepository.findById(id).orElseThrow(NotFoundException::new));
    }

    public DepositDto createDeposit(DepositSaveDto depositSaveDto, UUID id) {
        Deposit deposit = depositMapper.depositSaveDtoToDeposit(depositSaveDto);
        Client client = clientRepository.findById(id).orElseThrow(NotFoundException::new);
        deposit.setClient(client);
        Deposit deposit1 = depositRepository.save(deposit);
        return depositMapper.depositToDepositDto(deposit1);
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public DepositDto updateDeposit(DepositSaveDto updateDepositRequest, UUID id) {
        Deposit depositOld = depositRepository.findById(id).orElseThrow(NotFoundException::new);
        Integer depositOldValue = depositOld.getDeposit() + updateDepositRequest.getDeposit();
        depositOld.setDeposit(depositOldValue);
        return depositMapper.depositToDepositDto(
                depositRepository.save(depositOld));
    }

    public void deleteDepositById(UUID id) {
        depositRepository.delete(depositRepository.findById(id).orElseThrow(NotFoundException::new));
    }
}
