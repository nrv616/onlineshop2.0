package ru.nsecret.nrazgovorov.onlineshop.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.nsecret.nrazgovorov.onlineshop.exception.NotFoundException;
import ru.nsecret.nrazgovorov.onlineshop.model.Category;
import ru.nsecret.nrazgovorov.onlineshop.repository.CategoryRepository;
import ru.nsecret.nrazgovorov.onlineshop.rest.category.dto.CategoryDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.category.dto.CategorySaveDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.category.mapper.CategoryMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;

    private final CategoryMapper categoryMapper;

    public List<CategoryDto> getAllCategorys() {
        List<CategoryDto> categorys = new ArrayList<>();
        for (Category category : categoryRepository.findAll()) {
            categorys.add(categoryMapper.categoryToCategoryDtoMap(category));
        }
        return categorys;
    }

    public List<CategoryDto> getAllCategorysPaging(Pageable pageable) {
        List<CategoryDto> categorys = new ArrayList<>();
        for (Category category : categoryRepository.findAll(pageable)) {
            categorys.add(categoryMapper.categoryToCategoryDtoMap(category));
        }
        return categorys;
    }

    public CategoryDto getCategoryById(Integer id) {
        Category category = categoryRepository.findById(id).orElseThrow(NotFoundException::new);
        return categoryMapper.categoryToCategoryDtoMap(category);
    }

    public CategoryDto createCategory(CategorySaveDto categorySaveDto) {
        Category category = categoryMapper.categorySaveDtoToCategory(categorySaveDto);
        if (categorySaveDto.getParentId() != null) {
            Category parent = categoryRepository.findById(categorySaveDto.getParentId()).orElseThrow(NotFoundException::new);
            category.setParent(parent);
        }
        return categoryMapper.categoryToCategoryDtoMap(categoryRepository.save(category));
    }

    public CategoryDto updateCategory(CategorySaveDto updateCategoryRequest, Integer id) {
        Category categoryOld = categoryRepository.findById(id).orElseThrow(NotFoundException::new);
        if (updateCategoryRequest.getParentId() != null) {
            Category parent = categoryRepository.findById(updateCategoryRequest.getParentId()).orElseThrow(NotFoundException::new);
            categoryOld.setParent(parent);
        }
        categoryOld.setName(updateCategoryRequest.getName());

        return categoryMapper.categoryToCategoryDtoMap(
                categoryRepository.save(categoryOld));
    }

    public void deleteCategoryById(Integer id) {
        categoryRepository.delete(categoryRepository.findById(id).orElseThrow(NotFoundException::new));
    }
}
