package ru.nsecret.nrazgovorov.onlineshop.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.exception.NotFoundException;
import ru.nsecret.nrazgovorov.onlineshop.model.Category;
import ru.nsecret.nrazgovorov.onlineshop.model.Product;
import ru.nsecret.nrazgovorov.onlineshop.model.QProduct;
import ru.nsecret.nrazgovorov.onlineshop.repository.CategoryRepository;
import ru.nsecret.nrazgovorov.onlineshop.repository.ProductRepository;
import ru.nsecret.nrazgovorov.onlineshop.rest.product.dto.ProductDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.product.dto.ProductSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.product.dto.ReferenceCategoryDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.product.mapper.ProductMapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    private final CategoryRepository categoryRepository;

    private final ProductMapper productMapper;

    public List<ProductDto> getAllProductsPaging(Pageable pageable) {
        List<ProductDto> products = new ArrayList<>();
        for (Product product : productRepository.findAll(pageable)) {
            products.add(productMapper.productToProductDto(product));
        }
        return products;
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public List<Product> findByIds(List<Integer> ids) {
        return StreamSupport.stream(productRepository.findAll(QProduct.product.id.in(ids)).spliterator(), false)
                .collect(Collectors.toList());
    }

    public Product findById(Integer id) {
        return productRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public ProductDto getProductById(Integer id) {
        Product product = productRepository.findById(id).orElseThrow(NotFoundException::new);
        return productMapper.productToProductDto(product);
    }

    public ProductDto createProduct(ProductSaveDto productSaveDto) {
        Product product = productMapper.productSaveDtoToProduct(productSaveDto);
        Set<Category> categories = new HashSet<>();
        if (productSaveDto.getCategories() != null) {
            for (ReferenceCategoryDto referenceCategoryDto : productSaveDto.getCategories()) {
                categories.add(categoryRepository.findById(referenceCategoryDto.getId())
                        .orElseThrow(NotFoundException::new));
            }
        }
        product.setCategories(categories);
        return productMapper.productToProductDto(productRepository.save(product));
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public ProductDto updateProduct(ProductSaveDto updateProductRequest, Integer id) {
        Product productOld = productRepository.findById(id).orElseThrow(NotFoundException::new);
        productOld.setName(updateProductRequest.getName());
        productOld.setPrice(updateProductRequest.getPrice());
        productOld.setCount(updateProductRequest.getCount());
        Set<Category> categories = new HashSet<>();
        if (updateProductRequest.getCategories() != null) {
            for (ReferenceCategoryDto referenceCategoryDto : updateProductRequest.getCategories()) {
                categories.add(categoryRepository.findById(referenceCategoryDto.getId())
                        .orElseThrow(NotFoundException::new));
            }
        }
        productOld.setCategories(categories);
        return productMapper.productToProductDto(
                productRepository.save(productOld));
    }

    public void deleteProductById(Integer id) {
        productRepository.delete(productRepository.findById(id).orElseThrow(NotFoundException::new));
    }
}
