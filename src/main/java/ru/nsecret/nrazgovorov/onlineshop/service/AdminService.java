package ru.nsecret.nrazgovorov.onlineshop.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.nsecret.nrazgovorov.onlineshop.exception.NotFoundException;
import ru.nsecret.nrazgovorov.onlineshop.model.Admin;
import ru.nsecret.nrazgovorov.onlineshop.model.User;
import ru.nsecret.nrazgovorov.onlineshop.repository.AdminRepository;
import ru.nsecret.nrazgovorov.onlineshop.repository.UserRepository;
import ru.nsecret.nrazgovorov.onlineshop.rest.admin.dto.AdminDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.admin.dto.AdminSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.admin.mapper.AdminMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AdminService {

    private final AdminRepository adminRepository;

    private final UserRepository userRepository;

    private final AdminMapper adminMapper;

    public List<AdminDto> getAllAdmins() {
        List<AdminDto> admins = new ArrayList<>();
        for (Admin admin : adminRepository.findAll()) {
            admins.add(adminMapper.adminToAdminDto(admin));
        }
        return admins;
    }

    public List<AdminDto> getAllAdminsPaging(Pageable pageable) {
        List<AdminDto> admins = new ArrayList<>();
        for (Admin admin : adminRepository.findAll(pageable)) {
            admins.add(adminMapper.adminToAdminDto(admin));
        }
        return admins;
    }

    public AdminDto getAdminById(UUID id) {
        return adminMapper.adminToAdminDto(adminRepository.findById(id).orElseThrow(NotFoundException::new));
    }

    public AdminDto registerAdmin(AdminSaveDto adminSaveDto) {
        Admin admin = adminMapper.adminSaveDtoToAdmin(adminSaveDto);
        User user = userRepository.save(admin.getUser());
        admin.getUser().setId(user.getId());
        admin.setId(user.getId());
        return adminMapper.adminToAdminDto(adminRepository.save(admin));
    }

    public AdminDto updateAdmin(AdminSaveDto updateAdminRequest, UUID id) {
        Admin adminOld = adminRepository.findById(id).orElseThrow(NotFoundException::new);
        adminOld.getUser().setName(updateAdminRequest.getName());
        adminOld.setPosition(updateAdminRequest.getPosition());
        return adminMapper.adminToAdminDto(
                adminRepository.save(adminOld));
    }

    public void deleteAdminById(UUID id) {
        adminRepository.delete(adminRepository.findById(id).orElseThrow(NotFoundException::new));
        userRepository.delete(userRepository.findById(id).orElseThrow(NotFoundException::new));
    }
}
