package ru.nsecret.nrazgovorov.onlineshop.service;

import com.google.common.primitives.Floats;
import lombok.RequiredArgsConstructor;
import ml.dmlc.xgboost4j.java.Booster;
import ml.dmlc.xgboost4j.java.DMatrix;
import ml.dmlc.xgboost4j.java.XGBoost;
import ml.dmlc.xgboost4j.java.XGBoostError;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.nsecret.nrazgovorov.onlineshop.model.Product;
import ru.nsecret.nrazgovorov.onlineshop.rest.prediction.dto.PredictionDto;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PredictionService {

    public static final String MODEL_PATH = "C:\\Users\\nrazgovorov\\IdeaProjects\\omgtu\\vkr\\onlineshop2.0\\src\\main\\resources\\prediction\\model.bin";
    public static final int NCOL = 4;
    private final ProductService productService;

    @Value("${application.prediction.beginning-date}")
    private String beginningDateString;

    public List<PredictionDto> predictAll() throws XGBoostError {
        List<Product> products = productService.findAll();
        return predict(products);
    }

    public List<PredictionDto> predictById(Integer id) throws XGBoostError {
        Product product = productService.findById(id);
        return predict(Collections.singletonList(product));
    }

    public List<PredictionDto> predictByIds(List<Integer> ids) throws XGBoostError {
        List<Product> products = productService.findByIds(ids);
        return predict(products);
    }

    private List<PredictionDto> predict(List<Product> products) throws XGBoostError {
        LocalDate beginningDate = LocalDate.parse(beginningDateString);
        float predictionMonth = ChronoUnit.MONTHS.between(beginningDate, LocalDate.now()) + 1;

        List<Float> floats = new ArrayList<>();
        products.forEach(product -> Collections.addAll(floats, 0f, (float) product.getId(),
                (float) product.getCategories().stream().findFirst().get().getId(), predictionMonth));
        float[] data = Floats.toArray(floats);

        Booster booster = XGBoost.loadModel(MODEL_PATH);
        int nrow = products.size();
        DMatrix dmat = new DMatrix(data, nrow, NCOL);
        float[][] predicts = booster.predict(dmat);
        List<PredictionDto> predictionDtos = new ArrayList<>();
        for (int i = 0; i < predicts.length; i++) {
            predictionDtos.add(new PredictionDto(products.get(0).getId(), predicts[i][0]));
        }
        return predictionDtos;
    }

}
