package ru.nsecret.nrazgovorov.onlineshop.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.exception.AuthenticationException;
import ru.nsecret.nrazgovorov.onlineshop.exception.NotFoundException;
import ru.nsecret.nrazgovorov.onlineshop.model.User;
import ru.nsecret.nrazgovorov.onlineshop.repository.UserRepository;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.dto.UserDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.dto.UserSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.mapper.UserMapper;
import ru.nsecret.nrazgovorov.onlineshop.security.UserDetailsAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    private final AuthenticationManager authenticationManager;

    public List<UserDto> getAllUsers() {
        List<UserDto> users = new ArrayList<>();
        for (User user : userRepository.findAll()) {
            users.add(userMapper.userToUserDto(user));
        }
        return users;
    }

    public List<UserDto> getAllUsersPaging(Pageable pageable) {
        List<UserDto> users = new ArrayList<>();
        for (User user : userRepository.findAll(pageable)) {
            users.add(userMapper.userToUserDto(user));
        }
        return users;
    }

    public UserDto getUserById(UUID id) {
        return userMapper.userToUserDto(userRepository.findById(id).orElseThrow(NotFoundException::new));
    }

    public UserDto createUser(UserSaveDto userSaveDto) {
        User user1 = userMapper.userSaveDtoToUser(userSaveDto);
        User user = userRepository.save(user1);
        return userMapper.userToUserDto(user);
    }

    public UserDto updateUser(UserSaveDto updateUserRequest, UUID id) {
        User userOld = userRepository.findById(id).orElseThrow(NotFoundException::new);
        userOld.setName(updateUserRequest.getName());
        return userMapper.userToUserDto(
                userRepository.save(userOld));
    }

    private User getCurrentUserOrThrow() {
        return getCurrentUser().orElseThrow(() -> new AuthenticationException("A000006"));
    }

    public Optional<User> getCurrentUser() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .filter(Authentication::isAuthenticated)
                .map(Authentication::getPrincipal)
                .map(UserDetailsAdapter.class::cast)
                .map(UserDetailsAdapter::getUser);
    }


    public void deleteUserById(UUID id) {
        userRepository.delete(userRepository.findById(id).orElseThrow(NotFoundException::new));
    }

    @Transactional
    public void authenticate(String login, String password) {
        try {
            Authentication authenticationToken = new UsernamePasswordAuthenticationToken(login, password);
            Authentication successAuthentication = authenticationManager.authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(successAuthentication);
        } catch (org.springframework.security.core.AuthenticationException e) {
            throw new AuthenticationException("A000001");
        }
    }
}
