package ru.nsecret.nrazgovorov.onlineshop.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.nsecret.nrazgovorov.onlineshop.exception.NotFoundException;
import ru.nsecret.nrazgovorov.onlineshop.model.Client;
import ru.nsecret.nrazgovorov.onlineshop.model.User;
import ru.nsecret.nrazgovorov.onlineshop.repository.ClientRepository;
import ru.nsecret.nrazgovorov.onlineshop.repository.UserRepository;
import ru.nsecret.nrazgovorov.onlineshop.rest.client.dto.ClientDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.client.dto.ClientSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.client.mapper.ClientMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;

    private final UserRepository userRepository;

    private final ClientMapper clientMapper;

    public List<ClientDto> getAllClients() {
        List<ClientDto> clients = new ArrayList<>();
        for (Client client : clientRepository.findAll()) {
            clients.add(clientMapper.clientToClientDto(client));
        }
        return clients;
    }

    public List<ClientDto> getAllClientsPaging(Pageable pageable) {
        List<ClientDto> clients = new ArrayList<>();
        for (Client client : clientRepository.findAll(pageable)) {
            clients.add(clientMapper.clientToClientDto(client));
        }
        return clients;
    }

    public ClientDto getClientById(UUID id) {
        Client client = clientRepository.findById(id).orElseThrow(NotFoundException::new);
        return clientMapper.clientToClientDto(client);
    }

    public ClientDto createClient(ClientSaveDto clientSaveDto) {
        Client client = clientMapper.clientSaveDtoToClient(clientSaveDto);
        User user = userRepository.save(client.getUser());
        client.getUser().setId(user.getId());
        client.setId(user.getId());
        return clientMapper.clientToClientDto(clientRepository.save(client));
    }

    public ClientDto updateClient(ClientSaveDto updateClientRequest, UUID id) {
        Client clientOld = clientRepository.findById(id).orElseThrow(NotFoundException::new);
        clientOld.getUser().setName(updateClientRequest.getName());

        clientOld.setAddress(updateClientRequest.getAddress());
        clientOld.setEmail(updateClientRequest.getEmail());
        clientOld.setPhone(updateClientRequest.getPhone());
        return clientMapper.clientToClientDto(
                clientRepository.save(clientOld));
    }

    public void deleteClientById(UUID id) {
        clientRepository.delete(clientRepository.findById(id).orElseThrow(NotFoundException::new));
        userRepository.delete(userRepository.findById(id).orElseThrow(NotFoundException::new));
    }
}
