package ru.nsecret.nrazgovorov.onlineshop.rest.basket.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BasketSaveDto {

    private UUID clientId;

    private String name;

}
