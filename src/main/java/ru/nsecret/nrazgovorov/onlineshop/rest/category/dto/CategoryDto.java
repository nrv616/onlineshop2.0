package ru.nsecret.nrazgovorov.onlineshop.rest.category.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDto {

    private Integer id;

    private String name;

    private Integer parentId;

    private String parentName;

    private Set<CategoryDto> subCategories = new HashSet<>();

}
