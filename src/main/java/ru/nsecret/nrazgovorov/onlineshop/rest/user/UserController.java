package ru.nsecret.nrazgovorov.onlineshop.rest.user;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.dto.UserDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.dto.UserSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.service.UserService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/users")
    public List<UserDto> getAllUsers(Pageable pageable) {
        return userService.getAllUsersPaging(pageable);
    }

    @GetMapping("/users/{id}")
    public UserDto getUserById(@PathVariable("id") UUID id) {
        return userService.getUserById(id);
    }

    @PostMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> createUser(@RequestBody UserSaveDto userSaveDto) {
        return new ResponseEntity<>(userService.createUser(userSaveDto), HttpStatus.CREATED);
    }

    @PutMapping(value = "/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDto updateUser(@RequestBody UserSaveDto updateUserRequest, @PathVariable("id") UUID id) {
        return userService.updateUser(updateUserRequest, id);
    }

    @DeleteMapping("/users/{id}")
    public HttpStatus deleteUserById(@PathVariable("id") UUID id) {
        userService.deleteUserById(id);
        return HttpStatus.OK;
    }

}
