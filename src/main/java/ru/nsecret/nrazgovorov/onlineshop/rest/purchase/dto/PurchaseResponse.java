package ru.nsecret.nrazgovorov.onlineshop.rest.purchase.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseResponse {

    private UUID id;
    private String name;
    private int price;
    private int count;

}
