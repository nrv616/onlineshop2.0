package ru.nsecret.nrazgovorov.onlineshop.rest.client;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nsecret.nrazgovorov.onlineshop.rest.client.dto.ClientDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.client.dto.ClientSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.service.ClientService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;

    @GetMapping("/clients")
    public List<ClientDto> getAllClients(Pageable pageable) {
        return clientService.getAllClientsPaging(pageable);
    }

    @GetMapping("/clients/{id}")
    public ClientDto getClientById(@PathVariable("id") UUID id) {
        return clientService.getClientById(id);
    }

    @PostMapping(value = "/clients", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientDto> createClient(@RequestBody ClientSaveDto clientSaveDto) {
        return new ResponseEntity<>(clientService.createClient(clientSaveDto), HttpStatus.CREATED);
    }

    @PutMapping(value = "/clients/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ClientDto updateClient(@RequestBody ClientSaveDto updateClientRequest, @PathVariable("id") UUID id) {
        return clientService.updateClient(updateClientRequest, id);
    }

    @DeleteMapping("/clients/{id}")
    public HttpStatus deleteClientById(@PathVariable("id") UUID id) {
        clientService.deleteClientById(id);
        return HttpStatus.OK;
    }

}
