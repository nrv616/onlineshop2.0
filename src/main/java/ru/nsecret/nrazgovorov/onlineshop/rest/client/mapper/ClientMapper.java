package ru.nsecret.nrazgovorov.onlineshop.rest.client.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import ru.nsecret.nrazgovorov.onlineshop.model.Client;
import ru.nsecret.nrazgovorov.onlineshop.rest.client.dto.ClientDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.client.dto.ClientSaveDto;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ClientMapper {

    //    @Mapping(source = "name", target = "user.name")
//    @Mapping(source = "lastName", target = "user.lastName")
//    @Mapping(source = "middleName", target = "user.middleName")
//    @Mapping(source = "login", target = "user.login")
//    @Mapping(source = "password", target = "user.password")
    Client clientSaveDtoToClient(ClientSaveDto clientSaveDto);

    //    @Mapping(source = "user.id", target = "id")
//    @Mapping(source = "user.name", target = "name")
//    @Mapping(source = "user.lastName", target = "lastName")
//    @Mapping(source = "user.middleName", target = "middleName")
//    @Mapping(source = "user.login", target = "login")
//    @Mapping(source = "user.password", target = "password")
    @Mapping(source = "deposit.deposit", target = "deposit")
    ClientDto clientToClientDto(Client client);

    //    @Mapping(source = "id", target = "user.id")
//    @Mapping(source = "name", target = "user.name")
//    @Mapping(source = "lastName", target = "user.lastName")
//    @Mapping(source = "middleName", target = "user.middleName")
//    @Mapping(source = "login", target = "user.login")
//    @Mapping(source = "password", target = "user.password")
    @Mapping(source = "deposit", target = "deposit.deposit")
    Client clientDtoToClient(ClientDto clientDto);

}
