package ru.nsecret.nrazgovorov.onlineshop.rest.product;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nsecret.nrazgovorov.onlineshop.rest.product.dto.ProductDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.product.dto.ProductSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.service.ProductService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping("/products")
    public List<ProductDto> getAllProducts(Pageable pageable) {
        return productService.getAllProductsPaging(pageable);
    }

    @GetMapping("/products/{id}")
    public ProductDto getProductById(@PathVariable("id") Integer id) {
        return productService.getProductById(id);
    }

    @PostMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDto> createProduct(@RequestBody ProductSaveDto productSaveDto) {
        return new ResponseEntity<>(productService.createProduct(productSaveDto), HttpStatus.CREATED);
    }

    @PutMapping(value = "/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ProductDto updateProduct(@RequestBody ProductSaveDto updateProductRequest, @PathVariable("id") Integer id) {
        return productService.updateProduct(updateProductRequest, id);
    }

    @DeleteMapping("/products/{id}")
    public HttpStatus deleteProductById(@PathVariable("id") Integer id) {
        productService.deleteProductById(id);
        return HttpStatus.OK;
    }

}
