package ru.nsecret.nrazgovorov.onlineshop.rest.admin;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nsecret.nrazgovorov.onlineshop.rest.admin.dto.AdminDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.admin.dto.AdminSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.service.AdminService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class AdminController {

    private final AdminService adminService;

    @GetMapping("/admins")
    public List<AdminDto> getAllAdmins(Pageable pageable) {
        return adminService.getAllAdminsPaging(pageable);
    }

    @GetMapping("/admins/{id}")
    public AdminDto getAdminById(@PathVariable("id") UUID id) {
        return adminService.getAdminById(id);
    }

    @PostMapping(value = "/admins", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AdminDto> registerAdmin(@RequestBody AdminSaveDto adminSaveDto) {
        return new ResponseEntity<>(adminService.registerAdmin(adminSaveDto), HttpStatus.CREATED);
    }

    @PutMapping(value = "/admins/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public AdminDto updateAdmin(@RequestBody AdminSaveDto updateAdminRequest, @PathVariable("id") UUID id) {
        return adminService.updateAdmin(updateAdminRequest, id);
    }

    @DeleteMapping("/admins/{id}")
    public HttpStatus deleteAdminById(@PathVariable("id") UUID id) {
        adminService.deleteAdminById(id);
        return HttpStatus.OK;
    }

}
