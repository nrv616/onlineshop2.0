package ru.nsecret.nrazgovorov.onlineshop.rest.prediction.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PredictionDto {

    private Integer productId;

    private float itemSentNextMonthPrediction;
}
