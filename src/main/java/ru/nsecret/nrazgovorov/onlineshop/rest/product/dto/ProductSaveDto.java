package ru.nsecret.nrazgovorov.onlineshop.rest.product.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductSaveDto {

    @NotNull
    private String name;

    @NotNull
    private Integer price;

    private Integer count;

    private Set<ReferenceCategoryDto> categories;

}
