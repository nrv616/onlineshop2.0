package ru.nsecret.nrazgovorov.onlineshop.rest.auth;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsecret.nrazgovorov.onlineshop.rest.auth.dto.JwtRequest;
import ru.nsecret.nrazgovorov.onlineshop.rest.auth.dto.JwtResponse;
import ru.nsecret.nrazgovorov.onlineshop.security.CustomUserDetailsService;
import ru.nsecret.nrazgovorov.onlineshop.security.JwtTokenProvider;
import ru.nsecret.nrazgovorov.onlineshop.security.UserDetailsAdapter;
import ru.nsecret.nrazgovorov.onlineshop.service.UserService;

import java.util.Locale;

import static ru.nsecret.nrazgovorov.onlineshop.util.LocalizationUtils.createLocaleFromString;


@RestController
@RequiredArgsConstructor
public class AuthController {

    private final UserService userService;

    private final CustomUserDetailsService userDetailsService;

    private final JwtTokenProvider tokenProvider;

    @ApiOperation(value = "Аутентифицировать пользователя", notes = "Чтобы аутентифицировать пользователя передайте JSON c логином и паролем."
            + " Пользователю будет задана Cookie, необходимая для вызова других методов", produces = "application/json")
    @ApiResponses({
            @ApiResponse(response = JwtResponse.class, code = 200, message = "В случае успешной аутентификации"),
            @ApiResponse(code = 401, message = "В случае неудачной аутентификации")
    })
    @PostMapping("/auth")
    public JwtResponse login(
            @ApiParam(value = "Запрос на получение токена", required = true) @RequestBody JwtRequest req)
            throws Exception {
        userService.authenticate(req.getLogin(), req.getPassword());
        UserDetailsAdapter userDetails = (UserDetailsAdapter) userDetailsService.loadUserByUsername(req.getLogin());

        return tokenProvider.generateToken(userDetails);
    }
}
