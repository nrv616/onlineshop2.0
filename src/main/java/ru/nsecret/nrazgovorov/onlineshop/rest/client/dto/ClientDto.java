package ru.nsecret.nrazgovorov.onlineshop.rest.client.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientDto {

    private UUID id;

    private String name;

    private String lastName;

    private String middleName;

    private String login;

    private String password;

    private String email;

    private String address;

    private String phone;

    private Integer deposit;
}
