package ru.nsecret.nrazgovorov.onlineshop.rest.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserSaveDto {

    @NotNull
    private String name;

    @NotNull
    private String login;

    @NotNull
    private String password;
}
