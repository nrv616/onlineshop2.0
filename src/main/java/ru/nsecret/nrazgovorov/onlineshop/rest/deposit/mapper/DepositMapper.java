package ru.nsecret.nrazgovorov.onlineshop.rest.deposit.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import ru.nsecret.nrazgovorov.onlineshop.model.Deposit;
import ru.nsecret.nrazgovorov.onlineshop.rest.deposit.dto.DepositDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.deposit.dto.DepositSaveDto;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DepositMapper {

    Deposit depositSaveDtoToDeposit(DepositSaveDto depositSaveDto);

    DepositDto depositToDepositDto(Deposit deposit);

    Deposit depositDtoToDeposit(DepositDto clientDto);

}
