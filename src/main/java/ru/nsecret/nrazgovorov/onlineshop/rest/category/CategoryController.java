package ru.nsecret.nrazgovorov.onlineshop.rest.category;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nsecret.nrazgovorov.onlineshop.rest.category.dto.CategoryDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.category.dto.CategorySaveDto;
import ru.nsecret.nrazgovorov.onlineshop.service.CategoryService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping("/categories")
    public List<CategoryDto> getAllCategorys(Pageable pageable) {
        return categoryService.getAllCategorysPaging(pageable);
    }

    @GetMapping("/categories/{id}")
    public CategoryDto getCategoryById(@PathVariable("id") Integer id) {
        return categoryService.getCategoryById(id);
    }

    @PostMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CategoryDto> createCategory(@RequestBody CategorySaveDto categorySaveDto) {
        return new ResponseEntity<>(categoryService.createCategory(categorySaveDto), HttpStatus.CREATED);
    }

    @PutMapping(value = "/categories/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public CategoryDto updateCategory(@RequestBody CategorySaveDto updateCategoryRequest, @PathVariable("id") Integer id) {
        return categoryService.updateCategory(updateCategoryRequest, id);
    }

    @DeleteMapping("/categories/{id}")
    public HttpStatus deleteCategoryById(@PathVariable("id") Integer id) {
        categoryService.deleteCategoryById(id);
        return HttpStatus.OK;
    }

}
