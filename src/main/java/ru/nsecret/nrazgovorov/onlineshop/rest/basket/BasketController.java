package ru.nsecret.nrazgovorov.onlineshop.rest.basket;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsecret.nrazgovorov.onlineshop.rest.basket.dto.BasketDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.basket.dto.BasketProductDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.basket.dto.BasketSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.purchase.dto.PurchaseRequest;
import ru.nsecret.nrazgovorov.onlineshop.service.BasketService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/baskets")
public class BasketController {

    private final BasketService basketService;

    @GetMapping
    public List<BasketDto> getAllBaskets(Pageable pageable) {
        return basketService.getAllBasketsPaging(pageable);
    }

    @GetMapping("/{id}")
    public BasketDto getBasketById(@PathVariable("id") UUID id) {
        return basketService.getBasketById(id);
    }

    @PostMapping
    public BasketDto createBasket(@RequestBody BasketSaveDto basketSaveDto) {
        return basketService.createBasket(basketSaveDto);
    }

    @PutMapping(value = "/{id}")
    public BasketDto updateBasket(@RequestBody BasketSaveDto updateBasketRequest, @PathVariable("id") UUID id) {
        return basketService.updateBasket(updateBasketRequest, id);
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteBasketById(@PathVariable("id") UUID id) {
        basketService.deleteBasketById(id);
        return HttpStatus.OK;
    }

    @PostMapping("/product")
    public List<BasketProductDto> addProductToBasket(@Valid @RequestBody PurchaseRequest purchaseRequest) {
        return basketService.addProductToBasket(purchaseRequest);
    }

    @DeleteMapping("/product")
    public HttpStatus deleteProduct(@PathVariable("productId") UUID productId) {
        basketService.deleteProduct(productId);
        return HttpStatus.OK;
    }

    @PutMapping("/product")
    public List<BasketProductDto> editProductFromBasket(@Valid @RequestBody PurchaseRequest purchaseRequest) {
        return basketService.editProductFromBasket(purchaseRequest);
    }

    @GetMapping("/product")
    public List<BasketProductDto> getProductsFromBasket(HttpServletRequest request) {
        return basketService.getProductsFromBasket();
    }

}
