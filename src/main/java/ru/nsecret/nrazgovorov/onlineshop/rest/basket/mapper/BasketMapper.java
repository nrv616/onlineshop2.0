package ru.nsecret.nrazgovorov.onlineshop.rest.basket.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import ru.nsecret.nrazgovorov.onlineshop.model.Basket;
import ru.nsecret.nrazgovorov.onlineshop.rest.basket.dto.BasketDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.basket.dto.BasketSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.client.mapper.ClientMapper;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {BasketProductMapper.class})
public interface BasketMapper {

    Basket basketSaveDtoToBasket(BasketSaveDto basketSaveDto);

    BasketDto basketToBasketDto(Basket basket);

}
