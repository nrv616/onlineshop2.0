package ru.nsecret.nrazgovorov.onlineshop.rest.basket.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import ru.nsecret.nrazgovorov.onlineshop.model.BasketProduct;
import ru.nsecret.nrazgovorov.onlineshop.rest.basket.dto.BasketProductDto;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BasketProductMapper {

    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "product.name", target = "name")
    @Mapping(source = "product.price", target = "price")
    @Mapping(source = "count", target = "count")
    BasketProductDto basketProductToBasketProductDto(BasketProduct basketProduct);

}
