package ru.nsecret.nrazgovorov.onlineshop.rest.product.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import ru.nsecret.nrazgovorov.onlineshop.model.Product;
import ru.nsecret.nrazgovorov.onlineshop.rest.product.dto.ProductDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.product.dto.ProductSaveDto;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {ReferenceCategoryMapper.class})
public interface ProductMapper {

    @Mapping(target = "categories", ignore = true)
    Product productSaveDtoToProduct(ProductSaveDto productSaveDto);

    ProductDto productToProductDto(Product product);

    Product productDtoToProduct(ProductDto productDto);

}
