package ru.nsecret.nrazgovorov.onlineshop.rest.product.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import ru.nsecret.nrazgovorov.onlineshop.model.Category;
import ru.nsecret.nrazgovorov.onlineshop.rest.product.dto.ReferenceCategoryDto;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ReferenceCategoryMapper {

    @Mapping(source = "id", target = "id")
    ReferenceCategoryDto categoryToReferenceCategoryDto(Category category);

}
