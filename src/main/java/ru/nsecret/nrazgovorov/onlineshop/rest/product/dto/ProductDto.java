package ru.nsecret.nrazgovorov.onlineshop.rest.product.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

    private Integer id;

    private String name;

    private Integer price;

    private Integer count;

    private Set<ReferenceCategoryDto> categories = new HashSet<>();
}
