package ru.nsecret.nrazgovorov.onlineshop.rest.purchase;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nsecret.nrazgovorov.onlineshop.rest.purchase.dto.PurchaseDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.purchase.dto.PurchaseSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.service.PurchaseService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class PurchaseController {

    private final PurchaseService purchaseService;

    @GetMapping("/purchases")
    public List<PurchaseDto> getAllPurchases(Pageable pageable) {
        return purchaseService.getAllPurchasesPaging(pageable);
    }

    @GetMapping("/purchases/{id}")
    public PurchaseDto getPurchaseById(@PathVariable("id") UUID id) {
        return purchaseService.getPurchaseById(id);
    }

    @PostMapping(value = "/purchases", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PurchaseDto> createPurchase(@RequestBody PurchaseSaveDto purchaseSaveDto) {
        return new ResponseEntity<>(purchaseService.createPurchase(purchaseSaveDto), HttpStatus.CREATED);
    }

    @PutMapping(value = "/purchases/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public PurchaseDto updatePurchase(@RequestBody PurchaseSaveDto updatePurchaseRequest, @PathVariable("id") UUID id) {
        return purchaseService.updatePurchase(updatePurchaseRequest, id);
    }

    @DeleteMapping("/purchases/{id}")
    public HttpStatus deletePurchaseById(@PathVariable("id") UUID id) {
        purchaseService.deletePurchaseById(id);
        return HttpStatus.OK;
    }

}
