package ru.nsecret.nrazgovorov.onlineshop.rest.prediction;

import lombok.RequiredArgsConstructor;
import ml.dmlc.xgboost4j.java.XGBoostError;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nsecret.nrazgovorov.onlineshop.rest.prediction.dto.PredictionDto;
import ru.nsecret.nrazgovorov.onlineshop.service.PredictionService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class PredictionController {

    private final PredictionService predictionService;

    @GetMapping("/predictions")
    public List<PredictionDto> getAllPredictions() throws XGBoostError {
        return predictionService.predictAll();
    }

    @GetMapping("/predictions/{id}")
    public List<PredictionDto> getPredictionById(@PathVariable("id") Integer id) throws XGBoostError {
        return predictionService.predictById(id);
    }

    @PostMapping(value = "/predictions", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<PredictionDto> createPrediction(@RequestBody List<Integer> ids) throws XGBoostError {
        return predictionService.predictByIds(ids);
    }

}
