package ru.nsecret.nrazgovorov.onlineshop.rest.purchase.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import ru.nsecret.nrazgovorov.onlineshop.model.Purchase;
import ru.nsecret.nrazgovorov.onlineshop.rest.purchase.dto.PurchaseDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.purchase.dto.PurchaseSaveDto;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PurchaseMapper {

    @Mapping(target = "product.id", source = "productId")
    @Mapping(target = "client.id", source = "clientId")
    Purchase purchaseSaveDtoToPurchase(PurchaseSaveDto purchaseSaveDto);

    PurchaseDto purchaseToPurchaseDto(Purchase purchase);

}
