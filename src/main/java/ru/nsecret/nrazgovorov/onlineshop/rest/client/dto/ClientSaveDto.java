package ru.nsecret.nrazgovorov.onlineshop.rest.client.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientSaveDto {

    @NotNull
    private String name;

    @NotNull
    private String lastName;

    private String middleName;

    @NotNull
    private String login;

    @NotNull
    private String password;

    @NotNull
    private String email;

    @NotNull
    private String address;

    @NotNull
    private String phone;
}
