package ru.nsecret.nrazgovorov.onlineshop.rest.user.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import ru.nsecret.nrazgovorov.onlineshop.model.User;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.dto.UserDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.dto.UserSaveDto;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    User userSaveDtoToUser(UserSaveDto userSaveDto);

    UserDto userToUserDto(User user);

    User userDtoToUser(UserDto createUserResponse);

}
