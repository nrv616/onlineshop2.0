package ru.nsecret.nrazgovorov.onlineshop.rest.category.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import ru.nsecret.nrazgovorov.onlineshop.model.Category;
import ru.nsecret.nrazgovorov.onlineshop.rest.category.dto.CategoryDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.category.dto.CategorySaveDto;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CategoryMapper {

    Category categorySaveDtoToCategory(CategorySaveDto categorySaveDto);

    @Mapping(target = "parentId", source = "parent.id")
    @Mapping(target = "parentName", source = "parent.name")
    CategoryDto categoryToCategoryDtoMap(Category category);

    @Mapping(target = "parent.id", source = "parentId")
    @Mapping(target = "parent.name", source = "parentName")
    Category categoryDtoToCategoryMap(CategoryDto categoryDto);

}
