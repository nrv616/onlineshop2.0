package ru.nsecret.nrazgovorov.onlineshop.rest.deposit;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nsecret.nrazgovorov.onlineshop.rest.deposit.dto.DepositDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.deposit.dto.DepositSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.service.DepositService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class DepositController {

    private final DepositService depositService;

    @GetMapping("/deposits")
    public List<DepositDto> getAllDeposits(Pageable pageable) {
        return depositService.getAllDepositsPaging(pageable);
    }

    @GetMapping("/deposits/{id}")
    public DepositDto getDepositById(@PathVariable("id") UUID id) {
        return depositService.getDepositById(id);
    }

    @PostMapping(value = "/deposits/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DepositDto> createDeposit(@RequestBody DepositSaveDto depositSaveDto, @PathVariable("id") UUID id) {
        return new ResponseEntity<>(depositService.createDeposit(depositSaveDto, id), HttpStatus.CREATED);
    }

    @PutMapping(value = "/deposits/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public DepositDto updateDeposit(@RequestBody DepositSaveDto updateDepositRequest, @PathVariable("id") UUID id) {
        return depositService.updateDeposit(updateDepositRequest, id);
    }

    @DeleteMapping("/deposits/{id}")
    public HttpStatus deleteDepositById(@PathVariable("id") UUID id) {
        depositService.deleteDepositById(id);
        return HttpStatus.OK;
    }

}
