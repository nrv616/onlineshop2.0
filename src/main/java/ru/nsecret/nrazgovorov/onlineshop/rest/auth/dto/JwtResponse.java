package ru.nsecret.nrazgovorov.onlineshop.rest.auth.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JwtResponse {

    @ApiModelProperty(value = "Владелец токена", example = "user")
    String subject;

    @ApiModelProperty(value = "Время к которому истечет время жизни токена (в миллисекундах - since Unix Epoch)",
            example = "1579724849031")
    String expiresAt;

    @ApiModelProperty(value = "Время жизни токена в секундах", example = "18000")
    String expirationTime;

    @ApiModelProperty(value = "Токен")
    String token;
}
