package ru.nsecret.nrazgovorov.onlineshop.rest.deposit.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DepositDto {

    private UUID id;

    @NotNull
    private Integer deposit;
}
