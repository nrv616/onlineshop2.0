package ru.nsecret.nrazgovorov.onlineshop.rest.admin.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import ru.nsecret.nrazgovorov.onlineshop.model.Admin;
import ru.nsecret.nrazgovorov.onlineshop.rest.admin.dto.AdminDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.admin.dto.AdminSaveDto;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AdminMapper {


    Admin adminSaveDtoToAdmin(AdminSaveDto adminSaveDto);


    AdminDto adminToAdminDto(Admin admin);


    Admin adminDtoToAdmin(AdminDto adminDto);

}
