package ru.nsecret.nrazgovorov.onlineshop.rest.basket.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.nsecret.nrazgovorov.onlineshop.rest.client.dto.ClientDto;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BasketDto {

    private UUID id;

    private String name;

    private Set<BasketProductDto> products = new HashSet<>();

}
