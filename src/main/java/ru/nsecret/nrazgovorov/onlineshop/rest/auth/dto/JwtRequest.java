package ru.nsecret.nrazgovorov.onlineshop.rest.auth.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JwtRequest implements Serializable {

    private static final long serialVersionUID = 3317917203593170208L;

    @ApiModelProperty(value = "Логин")
    @JsonProperty(value = "login", required = true)
    private String login;

    @ApiModelProperty(value = "Пароль")
    @JsonProperty(value = "password", required = true)
    private String password;
}