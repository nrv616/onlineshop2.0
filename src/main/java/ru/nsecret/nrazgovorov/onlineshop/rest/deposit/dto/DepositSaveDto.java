package ru.nsecret.nrazgovorov.onlineshop.rest.deposit.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DepositSaveDto {

    @NotNull
    private Integer deposit;
}
