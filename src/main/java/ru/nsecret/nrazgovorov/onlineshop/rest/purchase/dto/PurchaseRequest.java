package ru.nsecret.nrazgovorov.onlineshop.rest.purchase.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseRequest {

    private Integer id;
    @NotBlank
    @NotNull
    private String name;
    private int price;
    private int count;

}
