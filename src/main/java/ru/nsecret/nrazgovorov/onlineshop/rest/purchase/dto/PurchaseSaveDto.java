package ru.nsecret.nrazgovorov.onlineshop.rest.purchase.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseSaveDto {

    @NotNull
    private Integer productId;

    @NotNull
    private String name;

    @NotNull
    private Integer price;

    private Integer count;

    private UUID clientId;

}
