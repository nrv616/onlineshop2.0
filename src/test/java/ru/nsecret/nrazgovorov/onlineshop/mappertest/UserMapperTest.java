package ru.nsecret.nrazgovorov.onlineshop.mappertest;

import org.junit.Assert;
import org.junit.Test;
import org.mapstruct.factory.Mappers;
import ru.nsecret.nrazgovorov.onlineshop.model.User;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.dto.UserDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.dto.UserSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.mapper.UserMapper;

import java.util.UUID;

public class UserMapperTest {

    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    User createUser() {
        return User.builder()
                .id(UUID.randomUUID())
                .name("ASF")
                .login("login")
                .password("password")
                .build();
    }


    @Test
    public void userSaveDtoToUserTest() {
        UserSaveDto userSaveDto = UserSaveDto.builder()
                .name("Владимир")
                .login("login")
                .password("password")
                .build();
        User user = userMapper.userSaveDtoToUser(userSaveDto);
        Assert.assertEquals(user.getName(), userSaveDto.getName());
        Assert.assertEquals(user.getLogin(), userSaveDto.getLogin());
        Assert.assertEquals(user.getPassword(), userSaveDto.getPassword());
    }

    @Test
    public void userToUserDtoTest() {
        User user = createUser();
        UserDto createUserResponse = userMapper.userToUserDto(user);
        Assert.assertEquals(user.getId(), createUserResponse.getId());
        Assert.assertEquals(user.getName(), createUserResponse.getName());
        Assert.assertEquals(user.getLogin(), createUserResponse.getLogin());
        Assert.assertEquals(user.getPassword(), createUserResponse.getPassword());
    }

    @Test
    public void userDtoToUserTest() {
        UserDto createUserResponse = UserDto.builder()
                .id(UUID.randomUUID())
                .name("Владимир")
                .login("login")
                .password("password")
                .build();
        User user = userMapper.userDtoToUser(createUserResponse);
        Assert.assertEquals(user.getId(), createUserResponse.getId());
        Assert.assertEquals(user.getName(), createUserResponse.getName());
        Assert.assertEquals(user.getLogin(), createUserResponse.getLogin());
        Assert.assertEquals(user.getPassword(), createUserResponse.getPassword());
    }

}
