package ru.nsecret.nrazgovorov.onlineshop.integrationtest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.exception.NotFoundException;
import ru.nsecret.nrazgovorov.onlineshop.model.User;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.dto.UserDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.dto.UserSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.mapper.UserMapper;
import ru.nsecret.nrazgovorov.onlineshop.service.UserService;

import java.util.List;

import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Transactional
public class UserTest {

    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    @Autowired
    private UserService userService;

    User createUser(String login) {
        return userMapper.userDtoToUser(userService.createUser(UserSaveDto.builder()
                .name("Владимир")
                .login(login)
                .password("password")
                .build()));
    }

    @Test
    public void createUserTest() {
        User user = createUser("loginTest1");
        Assert.assertEquals("Владимир", user.getName());
    }

    @Test
    public void updateUserTest() {
        User user = createUser("loginTest1");
        UserDto updateUserResponse = userService.updateUser(UserSaveDto.builder()
                .name("Николай")
                .password("password")
                .build(), user.getId());
        Assert.assertEquals("Николай", updateUserResponse.getName());
    }


    @Test
    public void getUserByIdTest() {
        User user = createUser("loginTest1");
        UserDto userDto = userService.getUserById(user.getId());
        Assert.assertEquals("Владимир", userDto.getName());
    }

    @Test
    public void deleteUserByIdTest() {
        User user = createUser("loginTest1");
        userService.deleteUserById(user.getId());
        try {
            userService.getUserById(user.getId());
            fail();
        } catch (NotFoundException e) {

        }

    }

    @Test
    public void getAllUsersTest() {
        createUser("loginTest1");
        createUser("loginTest2");
        createUser("loginTest3");
        List<UserDto> users = userService.getAllUsers();
        Assert.assertEquals(5, users.size());
    }

}
