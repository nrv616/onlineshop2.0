package ru.nsecret.nrazgovorov.onlineshop.repositorytest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.model.Client;
import ru.nsecret.nrazgovorov.onlineshop.model.Product;
import ru.nsecret.nrazgovorov.onlineshop.model.Purchase;
import ru.nsecret.nrazgovorov.onlineshop.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
public class PurchaseRepositoryTest {

    @PersistenceContext
    EntityManager entityManager;

    Client createClient(User user) {
        return Client.builder()
                .user(user)
                .email("email")
                .address("address")
                .phone("phone")
                .build();
    }

    User createUser(String login) {
        return User.builder()
                .name("ASF")
                .login(login)
                .password("password")
                .build();
    }

    Product createProduct(String name) {
        return Product.builder()
                .name(name)
                .count(10)
                .price(100)
                .build();
    }

    Purchase createPurchase(Client client, Product product) {
        return Purchase.builder()
                .client(client)
                .product(product)
                .name(product.getName())
                .price(product.getPrice())
                .count(product.getCount())
                .build();
    }


    @Test
    public void purchaseSaveAndFindTest() {
        User userNew = createUser("login");
        entityManager.persist(userNew);
        Client clientNew = createClient(userNew);
        entityManager.persist(clientNew);
        Product productNew = createProduct("name");
        entityManager.persist(productNew);
        Purchase purchase = createPurchase(clientNew, productNew);
        entityManager.persist(purchase);

        Purchase purchaseFounded = entityManager.find(Purchase.class, purchase.getId());
        Assert.assertEquals(purchase.getId(), purchaseFounded.getId());
        Assert.assertEquals(purchase.getClient().getId(), purchaseFounded.getClient().getId());
        Assert.assertEquals(purchase.getProduct().getId(), purchase.getProduct().getId());

    }

    @Test
    public void purchaseDeleteTest() {
        User userNew = createUser("login");
        entityManager.persist(userNew);
        Client clientNew = createClient(userNew);
        entityManager.persist(clientNew);
        Product productNew = createProduct("name");
        entityManager.persist(productNew);
        Purchase purchase = createPurchase(clientNew, productNew);
        entityManager.persist(purchase);
        entityManager.remove(purchase);
        Purchase purchaseFounded = entityManager.find(Purchase.class, purchase.getId());
        Assert.assertNull(purchaseFounded);
    }


}
