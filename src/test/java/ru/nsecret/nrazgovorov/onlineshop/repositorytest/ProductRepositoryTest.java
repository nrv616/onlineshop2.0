package ru.nsecret.nrazgovorov.onlineshop.repositorytest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.model.Category;
import ru.nsecret.nrazgovorov.onlineshop.model.Product;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
public class ProductRepositoryTest {

    @PersistenceContext
    EntityManager entityManager;

    Product createProduct(String name) {
        return Product.builder()
                .name(name)
                .count(10)
                .price(100)
                .build();
    }

    Category createCategory(String name) {
        return Category.builder()
                .name(name)
                .build();
    }


    @Test
    public void productSaveAndFindTest() {
        Product productNew = Product.builder()
                .name("name")
                .count(10)
                .price(100)
                .build();
        entityManager.persist(productNew);
        Product productFounded = entityManager.find(Product.class, productNew.getId());
        Assert.assertEquals(productNew.getId(), productFounded.getId());
        Assert.assertEquals(productNew.getName(), productFounded.getName());
        Assert.assertEquals(productNew.getCount(), productFounded.getCount());
        Assert.assertEquals(productNew.getPrice(), productFounded.getPrice());

    }

    @Test
    public void productWithCategoriesSaveAndFindTest() {
        Category categoryNew1 = createCategory("parent");
        entityManager.persist(categoryNew1);
        Category categoryNew2 = Category.builder()
                .name("child")
                .parent(categoryNew1)
                .build();
        entityManager.persist(categoryNew2);
        Product productNew = createProduct("product");
        Set<Category> categories = new HashSet<>();
        categories.add(categoryNew1);
        categories.add(categoryNew2);
        productNew.setCategories(categories);
        entityManager.persist(productNew);
        Product productFounded = entityManager.find(Product.class, productNew.getId());
        Assert.assertEquals(productFounded.getId(), productFounded.getId());
        Assert.assertEquals(productFounded.getCategories().size(), 2);
    }

    @Test
    public void productDeleteTest() {
        Product productNew = createProduct("name");
        entityManager.persist(productNew);
        entityManager.remove(productNew);
        Product productFounded = entityManager.find(Product.class, productNew.getId());
        Assert.assertNull(productFounded);
    }

    @Test
    public void productSaveAndFindFewProductsTest() {
        Product productNew1 = createProduct("name1");
        Product productNew2 = createProduct("name2");
        Product productNew3 = createProduct("name3");
        entityManager.persist(productNew1);
        entityManager.persist(productNew2);
        entityManager.persist(productNew3);
        Product productFounded1 = entityManager.find(Product.class, productNew1.getId());
        Product productFounded2 = entityManager.find(Product.class, productNew2.getId());
        Product productFounded3 = entityManager.find(Product.class, productNew3.getId());

        Assert.assertEquals(productNew1.getId(), productFounded1.getId());
        Assert.assertEquals(productNew2.getId(), productFounded2.getId());
        Assert.assertEquals(productNew3.getId(), productFounded3.getId());
    }


}
