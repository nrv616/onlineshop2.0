package ru.nsecret.nrazgovorov.onlineshop.repositorytest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.model.Basket;
import ru.nsecret.nrazgovorov.onlineshop.model.BasketProduct;
import ru.nsecret.nrazgovorov.onlineshop.model.Client;
import ru.nsecret.nrazgovorov.onlineshop.model.Product;
import ru.nsecret.nrazgovorov.onlineshop.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
public class BasketRepositoryTest {

    @PersistenceContext
    EntityManager entityManager;

    Client createClient(User user) {
        return Client.builder()
                .user(user)
                .email("email")
                .address("address")
                .phone("phone")
                .build();
    }

    User createUser(String login) {
        return User.builder()
                .name("ASF")
                .login(login)
                .password("password")
                .build();
    }

    Product createProduct(String name) {
        return Product.builder()
                .name(name)
                .count(10)
                .price(100)
                .build();
    }

    Basket createBasket(Client client) {
        return Basket.builder()
                .clientId(client.getId())
                .build();
    }


    @Test
    public void basketWirhProductsSaveAndFindTest() {
        User userNew = createUser("login");
        entityManager.persist(userNew);
        Client clientNew = createClient(userNew);
        entityManager.persist(clientNew);
        Product productNew = createProduct("name");
        entityManager.persist(productNew);
        Basket basket = createBasket(clientNew);
        entityManager.persist(basket);
        BasketProduct basketProduct = BasketProduct.builder().basket(basket).count(10).product(productNew).build();
        entityManager.persist(basketProduct);

        Basket basketFounded = entityManager.find(Basket.class, basket.getClientId());
        BasketProduct basketProductFounded = entityManager.find(BasketProduct.class, basketProduct.getId());
        Assert.assertEquals(basket.getClientId(), basketFounded.getClientId());
        Assert.assertEquals(basketProductFounded.getCount(), Integer.valueOf(10));

    }

    @Test
    public void basketDeleteTest() {
        User userNew = createUser("login");
        entityManager.persist(userNew);
        Client clientNew = createClient(userNew);
        entityManager.persist(clientNew);
        Basket basket = createBasket(clientNew);
        entityManager.persist(basket);
        entityManager.remove(basket);
        Basket basketFounded = entityManager.find(Basket.class, basket.getClientId());
        Assert.assertNull(basketFounded);
    }


}
