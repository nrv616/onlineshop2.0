package ru.nsecret.nrazgovorov.onlineshop.repositorytest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.model.Client;
import ru.nsecret.nrazgovorov.onlineshop.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
public class ClientRepositoryTest {

    @PersistenceContext
    EntityManager entityManager;

    Client createClient(User user) {
        return Client.builder()
                .user(user)
                .email("email")
                .address("address")
                .phone("phone")
                .build();
    }

    User createUser(String login) {
        return User.builder()
                .name("ASF")
                .login(login)
                .password("password")
                .build();
    }


    @Test
    public void clientSaveAndFindTest() {
        User userNew = createUser("login");
        entityManager.persist(userNew);
        Client clientNew = Client.builder()
                .user(userNew)
                .email("email")
                .address("address")
                .phone("phone")
                .build();
        entityManager.persist(clientNew);
        Client clientFounded = entityManager.find(Client.class, clientNew.getId());
        Assert.assertEquals(clientNew.getId(), clientFounded.getId());
        Assert.assertEquals(clientNew.getUser().getName(), clientFounded.getUser().getName());
        Assert.assertEquals(clientNew.getUser().getLogin(), clientFounded.getUser().getLogin());
        Assert.assertEquals(clientNew.getUser().getPassword(), clientFounded.getUser().getPassword());
        Assert.assertEquals(clientNew.getEmail(), clientFounded.getEmail());
        Assert.assertEquals(clientNew.getPhone(), clientFounded.getPhone());
        Assert.assertEquals(clientNew.getAddress(), clientFounded.getAddress());
    }

    @Test
    public void clientDeleteTest() {
        User userNew = createUser("login");
        entityManager.persist(userNew);
        Client clientNew = createClient(userNew);
        entityManager.persist(clientNew);
        entityManager.remove(clientNew);
        Client clientFounded = entityManager.find(Client.class, clientNew.getId());
        Assert.assertNull(clientFounded);
    }

    @Test
    public void clientSaveAndFindFewUsersTest() {
        User userNew1 = createUser("login1");
        User userNew2 = createUser("login2");
        User userNew3 = createUser("login3");
        entityManager.persist(userNew1);
        entityManager.persist(userNew2);
        entityManager.persist(userNew3);
        Client clientNew1 = createClient(userNew1);
        entityManager.persist(clientNew1);
        Client clientNew2 = createClient(userNew2);
        entityManager.persist(clientNew2);
        Client clientNew3 = createClient(userNew3);
        entityManager.persist(clientNew3);
        Client clientFounded1 = entityManager.find(Client.class, clientNew1.getId());
        Client clientFounded2 = entityManager.find(Client.class, clientNew2.getId());
        Client clientFounded3 = entityManager.find(Client.class, clientNew3.getId());

        Assert.assertEquals(clientNew1.getId(), clientFounded1.getId());
        Assert.assertEquals(clientNew2.getId(), clientFounded2.getId());
        Assert.assertEquals(clientNew3.getId(), clientFounded3.getId());
    }


}
