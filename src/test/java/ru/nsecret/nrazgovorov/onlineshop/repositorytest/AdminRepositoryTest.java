package ru.nsecret.nrazgovorov.onlineshop.repositorytest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.model.Admin;
import ru.nsecret.nrazgovorov.onlineshop.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
public class AdminRepositoryTest {

    @PersistenceContext
    EntityManager entityManager;

    Admin registerAdmin(User user) {
        return Admin.builder()
                .user(user)
                .position("position")
                .build();
    }

    User createUser(String login) {
        return User.builder()
                .name("ASF")
                .login(login)
                .password("password")
                .build();
    }


    @Test
    public void adminSaveAndFindTest() {
        User userNew = createUser("login");
        entityManager.persist(userNew);
        Admin adminNew = Admin.builder()
                .user(userNew)
                .position("position")
                .build();
        entityManager.persist(adminNew);
        Admin adminFounded = entityManager.find(Admin.class, adminNew.getId());
        Assert.assertEquals(adminNew.getId(), adminFounded.getId());
        Assert.assertEquals(adminNew.getUser().getName(), adminFounded.getUser().getName());
        Assert.assertEquals(adminNew.getUser().getLogin(), adminFounded.getUser().getLogin());
        Assert.assertEquals(adminNew.getUser().getPassword(), adminFounded.getUser().getPassword());
        Assert.assertEquals(adminNew.getPosition(), adminFounded.getPosition());
    }

    @Test
    public void adminDeleteTest() {
        User userNew = createUser("login");
        entityManager.persist(userNew);
        Admin adminNew = registerAdmin(userNew);
        entityManager.persist(adminNew);
        entityManager.remove(adminNew);
        Admin adminFounded = entityManager.find(Admin.class, adminNew.getId());
        Assert.assertNull(adminFounded);
    }

    @Test
    public void adminSaveAndFindFewUsersTest() {
        User userNew1 = createUser("login1");
        User userNew2 = createUser("login2");
        User userNew3 = createUser("login3");
        entityManager.persist(userNew1);
        entityManager.persist(userNew2);
        entityManager.persist(userNew3);
        Admin adminNew1 = registerAdmin(userNew1);
        entityManager.persist(adminNew1);
        Admin adminNew2 = registerAdmin(userNew2);
        entityManager.persist(adminNew2);
        Admin adminNew3 = registerAdmin(userNew3);
        entityManager.persist(adminNew3);
        Admin adminFounded1 = entityManager.find(Admin.class, adminNew1.getId());
        Admin adminFounded2 = entityManager.find(Admin.class, adminNew2.getId());
        Admin adminFounded3 = entityManager.find(Admin.class, adminNew3.getId());

        Assert.assertEquals(adminNew1.getId(), adminFounded1.getId());
        Assert.assertEquals(adminNew2.getId(), adminFounded2.getId());
        Assert.assertEquals(adminNew3.getId(), adminFounded3.getId());

    }


}
