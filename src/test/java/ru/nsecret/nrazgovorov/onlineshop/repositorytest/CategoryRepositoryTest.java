package ru.nsecret.nrazgovorov.onlineshop.repositorytest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.model.Category;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
public class CategoryRepositoryTest {

    @PersistenceContext
    EntityManager entityManager;

    Category createCategory(String name) {
        return Category.builder()
                .name(name)
                .build();
    }


    @Test
    public void categorySaveAndFindTest() {
        Category categoryNew = Category.builder()
                .name("name")
                .build();
        entityManager.persist(categoryNew);
        Category categoryFounded = entityManager.find(Category.class, categoryNew.getId());
        Assert.assertEquals(categoryNew.getId(), categoryFounded.getId());
        Assert.assertEquals(categoryNew.getName(), categoryFounded.getName());

    }

    @Test
    public void categoryWithParentSaveAndFindTest() {
        Category categoryNew1 = createCategory("parent");
        entityManager.persist(categoryNew1);
        Category categoryNew2 = Category.builder()
                .name("child")
                .parent(categoryNew1)
                .build();
        entityManager.persist(categoryNew2);
        entityManager.flush();
        Category categoryFounded1 = entityManager.find(Category.class, categoryNew1.getId());
        Category categoryFounded2 = entityManager.find(Category.class, categoryNew2.getId());
        Assert.assertEquals(categoryFounded2.getParent().getId(), categoryFounded1.getId());
    }

    @Test
    public void categoryDeleteTest() {
        Category categoryNew = createCategory("name");
        entityManager.persist(categoryNew);
        entityManager.remove(categoryNew);
        Category categoryFounded = entityManager.find(Category.class, categoryNew.getId());
        Assert.assertNull(categoryFounded);
    }

    @Test
    public void categorySaveAndFindFewCategorysTest() {
        Category categoryNew1 = createCategory("name1");
        Category categoryNew2 = createCategory("name2");
        Category categoryNew3 = createCategory("name3");
        entityManager.persist(categoryNew1);
        entityManager.persist(categoryNew2);
        entityManager.persist(categoryNew3);
        Category categoryFounded1 = entityManager.find(Category.class, categoryNew1.getId());
        Category categoryFounded2 = entityManager.find(Category.class, categoryNew2.getId());
        Category categoryFounded3 = entityManager.find(Category.class, categoryNew3.getId());

        Assert.assertEquals(categoryNew1.getId(), categoryFounded1.getId());
        Assert.assertEquals(categoryNew2.getId(), categoryFounded2.getId());
        Assert.assertEquals(categoryNew3.getId(), categoryFounded3.getId());
    }


}
