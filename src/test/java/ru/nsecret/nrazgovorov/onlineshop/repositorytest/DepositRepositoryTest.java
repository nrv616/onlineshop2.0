package ru.nsecret.nrazgovorov.onlineshop.repositorytest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.model.Client;
import ru.nsecret.nrazgovorov.onlineshop.model.Deposit;
import ru.nsecret.nrazgovorov.onlineshop.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
public class DepositRepositoryTest {

    @PersistenceContext
    EntityManager entityManager;

    Client createClient(User user) {
        return Client.builder()
                .user(user)
                .email("email")
                .address("address")
                .phone("phone")
                .build();
    }

    Deposit createDeposit(Client client) {
        return Deposit.builder()
                .client(client)
                .deposit(1000)
                .build();
    }

    User createUser(String login) {
        return User.builder()
                .name("ASF")
                .login(login)
                .password("password")
                .build();
    }


    @Test
    public void depositSaveAndFindTest() {
        User userNew = createUser("login");
        entityManager.persist(userNew);
        Client clentNew = createClient(userNew);
        entityManager.persist(clentNew);
        Deposit deposit = createDeposit(clentNew);
        entityManager.persist(deposit);
        Client clientFounded = entityManager.find(Client.class, deposit.getClient().getId());
        Deposit depositFounded = entityManager.find(Deposit.class, deposit.getId());
        Assert.assertEquals(depositFounded.getClient().getId(), clientFounded.getId());
        Assert.assertEquals(depositFounded.getClient().getUser().getName(), clientFounded.getUser().getName());
        Assert.assertEquals(depositFounded.getClient().getUser().getLogin(), clientFounded.getUser().getLogin());
        Assert.assertEquals(depositFounded.getClient().getUser().getPassword(), clientFounded.getUser().getPassword());
        Assert.assertEquals(depositFounded.getClient().getEmail(), clientFounded.getEmail());
        Assert.assertEquals(depositFounded.getClient().getPhone(), clientFounded.getPhone());
        Assert.assertEquals(depositFounded.getClient().getAddress(), clientFounded.getAddress());
        Assert.assertEquals(depositFounded.getDeposit(), deposit.getDeposit());
    }

    @Test
    public void depositDeleteTest() {
        User userNew = createUser("login");
        entityManager.persist(userNew);
        Client clientNew = createClient(userNew);
        entityManager.persist(clientNew);
        Deposit deposit = createDeposit(clientNew);
        entityManager.persist(deposit);
        entityManager.remove(deposit);
        Deposit depositFounded = entityManager.find(Deposit.class, deposit.getId());
        Assert.assertNull(depositFounded);
    }

}
