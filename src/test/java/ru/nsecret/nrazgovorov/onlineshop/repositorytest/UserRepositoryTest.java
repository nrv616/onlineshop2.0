package ru.nsecret.nrazgovorov.onlineshop.repositorytest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nsecret.nrazgovorov.onlineshop.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
public class UserRepositoryTest {

    @PersistenceContext
    EntityManager entityManager;

    User createUser(String login) {
        return User.builder()
                .name("ASF")
                .login(login)
                .password("password")
                .build();
    }


    @Test
    public void userSaveAndFindTest() {
        User userNew = User.builder()
                .name("ASF")
                .login("login")
                .password("password")
                .build();
        entityManager.persist(userNew);
        User userFounded = entityManager.find(User.class, userNew.getId());
        Assert.assertEquals(userNew.getId(), userFounded.getId());
        Assert.assertEquals(userNew.getName(), userFounded.getName());
        Assert.assertEquals(userNew.getLogin(), userFounded.getLogin());
        Assert.assertEquals(userNew.getPassword(), userFounded.getPassword());
    }

    @Test
    public void userDeleteTest() {
        User userNew = createUser("login");
        entityManager.persist(userNew);
        entityManager.remove(userNew);
        User userFounded = entityManager.find(User.class, userNew.getId());
        Assert.assertNull(userFounded);
    }

    @Test
    public void userSaveAndFindFewUsersTest() {
        User userNew1 = createUser("login1");
        User userNew2 = createUser("login2");
        User userNew3 = createUser("login3");
        entityManager.persist(userNew1);
        entityManager.persist(userNew2);
        entityManager.persist(userNew3);
        User userFounded1 = entityManager.find(User.class, userNew1.getId());
        User userFounded2 = entityManager.find(User.class, userNew2.getId());
        User userFounded3 = entityManager.find(User.class, userNew3.getId());

        Assert.assertEquals(userNew1.getId(), userFounded1.getId());
        Assert.assertEquals(userNew2.getId(), userFounded2.getId());
        Assert.assertEquals(userNew3.getId(), userFounded3.getId());
    }


}
