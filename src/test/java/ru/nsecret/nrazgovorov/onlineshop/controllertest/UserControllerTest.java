package ru.nsecret.nrazgovorov.onlineshop.controllertest;

import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.nsecret.nrazgovorov.onlineshop.model.User;
import ru.nsecret.nrazgovorov.onlineshop.repository.UserRepository;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.UserController;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.dto.UserDto;
import ru.nsecret.nrazgovorov.onlineshop.rest.user.dto.UserSaveDto;
import ru.nsecret.nrazgovorov.onlineshop.security.CustomUserDetailsService;
import ru.nsecret.nrazgovorov.onlineshop.service.UserService;

import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
@AutoConfigureMockMvc(addFilters = false)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserService userService;

    @MockBean
    CustomUserDetailsService customUserDetailsService;

    @Test
    void getAllUsersTest() throws Exception {
        when(userService.getAllUsers()).thenReturn(new ArrayList<>());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/users")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"));
    }

    @Test
    void getUserByIdTest() throws Exception {
        when(userService.getUserById(any())).thenReturn(UserDto.builder().name("Vladimir").build());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/users/2595ecc6-3c67-42ee-b00b-f6f07ab7c075")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.name").value("Vladimir"));
    }

    @Test
    void createUserTest() throws Exception {
        when(userService.createUser(any())).thenReturn(UserDto.builder().name("Vladimir").build());
        this.mockMvc.perform(MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(User.builder().name("Vladimir").build()))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.name").value("Vladimir"));
    }

    @Test
    void updateUserTest() throws Exception {
        when(userService.updateUser(any(UserSaveDto.class), any())).thenReturn(UserDto.builder().name("Vladimir").build());
        this.mockMvc.perform(MockMvcRequestBuilders.put("/users/2595ecc6-3c67-42ee-b00b-f6f07ab7c075")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(User.builder().name("Vladimir").build()))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.name").value("Vladimir"));
    }

    @Test
    void deleteUserTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/users/2595ecc6-3c67-42ee-b00b-f6f07ab7c075")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
