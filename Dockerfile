FROM openjdk:11-jdk-alpine
ADD target/onlineshop-1.0-SNAPSHOT.jar onlineshop-1.0-SNAPSHOT.jar
EXPOSE 8888
ENTRYPOINT ["java","-jar","onlineshop-1.0-SNAPSHOT.jar"]