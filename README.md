# onlineshop2.0
Getting Started

**Requirements**

java 11
maven 3
docker 17.09.0+

**Build from source**
```
$ git clone https://gitlab.com/nrv616/onlineshop2.0.git
$ cd onlineshop/
$ mvn package spring-boot:repackage -DskipTests
```

После этого будет собран jar файл по пути ./target/onlineshop-1.0-SNAPSHOT.jar

**Run**
Для запуска приложения необходим postgres в docker.

Для создания контейнера postgres БД:
```
$ docker run --name onlineshop-psql -p 5432:5432 -e POSTGRES_DB=onlineshop
/ -e POSTGRES_PASSWORD=onlineshop -d postgres
```


Для использования уже существующего сервера БД postgres
в src/main/application/yaml поменяйте значения:
```
url: jdbc:postgresql://localhost:PortOfYourPostgres/onlineshop
    username: username
    password: password
```

    
Далее в onlineshop/ в терминале:
`$ java -jar target/onlineshop-1.0-SNAPSHOT.jar`
На localhost:8888 будет запущен веб сервер onlineshop с сообщенеим "Hello World!"
Откройте http://localhost:8888/swagger-ui.html

**Build docker image & Run**
Для сборки docker image необходимо выполнить [Build from source][#build-from-source]

при сборке docker image с приложением docker использует ./target/onlineshop-1.0-SNAPSHOT.jar
убедитесь, что он создан после выполнения [Build from source][#build-from-source]

Вариант 1:
```
 $ docker  build -t nrazgovorov/onlineshop ./
 $ docker container run -it --publish 8888:8888 --detach --name onlineshop -e SPRING.DATASOURCE.URL=jdbc:postgresql://onlineshop-psql:5432/onlineshop nrazgovorov/onlineshop
 $ docker network create onlineshop_net
 $ docker network connect onlineshop_net onlineshop
 $ docker network connect onlineshop_net onlineshop-psql
```

 Вариант 2:
```
 $ docker  build -t nrazgovorov/onlineshop ./
 $ docker container run -it --publish 8888:8888 --detach --name onlineshop --link onlineshop-psql nrazgovorov/onlineshop
```
**Run in docker-compose**
Для запуска через docker-compose необходимо выполнить [Build from source][#build-from-source]

при сборке docker image с приложением docker использует ./target/onlineshop-1.0-SNAPSHOT.jarr убедитесь,
что он создан после выполнения [Build from source][#build-from-source]

Пересобрать image и запустить контейнеры

`$ docker-compose up --build`

Остановить container'ы и удалить container'ы, network'и, volume'ы, and image'и созданные при вызове docker-compose up.

`$ docker-compose down`